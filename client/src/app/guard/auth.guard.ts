import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate  {

  constructor(private router: Router){  }
  canActivate(route:ActivatedRouteSnapshot, state: RouterStateSnapshot){
    //console.log({"route":route});
    //console.log({"state":state.url});
    let user = JSON.parse(sessionStorage.getItem('loggedInUser'));
    if(user){
      if((state.url==='/all-claim' || state.url==='/pending-claim' || state.url==='/all-claim' || state.url==='/add-user' || state.url==='/users-list') && user.data.role ==='user'){
        return this.router.parseUrl('/my-claims');
      }else if(( state.url==='/add-user' || state.url==='/users-list') && user.data.role ==='manager'){
        return this.router.parseUrl('/pending-claim');
      }else{
        return true;
      }
    }else{
      return this.router.parseUrl('/login');
    }
  }
}
