import { Component, OnInit, ElementRef } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FileUploader} from "ng2-file-upload";
import {Observable} from "rxjs";
import {ClaimRequestService} from "../services/claim-request.service";
import { async } from 'q';


@Component({
  selector: 'app-reimbursement-form',
  templateUrl: './reimbursement-form.component.html',
  styleUrls: ['./reimbursement-form.component.css']
})
export class ReimbursementFormComponent implements OnInit {
  isSubmit=false;
  filesName = '';
  constructor(private fb: FormBuilder, public claim : ClaimRequestService, private el: ElementRef) { }

  ngOnInit() {
    this.uploadForm = this.fb.group({
      document: [null,Validators.compose([Validators.required])],
      type:  [null, Validators.compose([Validators.required])],
      month:  [null, Validators.compose([Validators.required])],
      amount: [null, Validators.compose([Validators.required])],
      comment: [null, Validators.compose([Validators.required])]
    });
  }

  
  uploadForm: FormGroup;

  public uploader:FileUploader = new FileUploader({
    isHTML5: true
  });

  uploadSubmit(){
    this.isSubmit=true;
    if(this.uploadForm.valid){

    let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#document');
        let fileCount: number = inputEl.files.length;
       // let formData = new FormData();
       let loggedInUser = JSON.parse(sessionStorage.loggedInUser).data;
       let datetime = new Date();
        let formData = {  
                        type:this.uploadForm.controls.type.value, 
                        month:this.uploadForm.controls.month.value, 
                        amount:this.uploadForm.controls.amount.value,
                        comment:this.uploadForm.controls.comment.value,
                        status:"pending",
                        user : loggedInUser.email,
                        manager : loggedInUser.manager || null,
                        docs:[],
                        filename:[],
                        createddate:datetime,
                        updateddate:datetime
                      }
        if (fileCount && fileCount > 0) { // a file was selected
          let fileSize: number = inputEl.files[0].size;
          if(fileSize>0){
            let resolved = 0;
            for (let id in inputEl.files) {
              let _id = Number(id);
              let file:File = inputEl.files.item(Number(_id));
              let myReader:FileReader = new FileReader();
              myReader.onloadend = (file) => {
                if(inputEl.files[_id].name){
                  let fileName = new Date().getTime()+"_"+_id+"_"+inputEl.files[_id].name;
                  formData.docs.push({base64:myReader.result, type:file.type, "name":fileName});
                  formData.filename.push(fileName);
                  resolved++;
                  if((fileCount)==Number(resolved)){
                    this.claim.claimRequest(formData).subscribe((response:any)=>{
                      this.isSubmit = false;
                      //console.log(response);
                      if(response.status && response.status===1){
                        this.uploadForm.reset();
                        this.filesName='';
                        alert('Request submitted..');
                      }else{
                        this.uploadForm.reset();
                        this.filesName=''
                        alert('something went wrong please after sometime or contact to admin..');
                      }
                    })
                  }   
                }         
              }
              myReader.readAsDataURL(file);

            }
          }
        }
    }

  }

  uploadFile(data) {
    //console.log(data);
    if(data){
      return data;
    }
    return "";
  }


  fileChange(event){
    if(event.target.files.length===1){
      this.filesName=event.target.files[0].name
    }else if(event.target.files.length>1){
      this.filesName= event.target.files.length+" files selected"
    }
  }
}
