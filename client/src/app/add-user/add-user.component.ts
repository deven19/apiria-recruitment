import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormArray, FormControl, NgForm, NgModel } from "@angular/forms";
import { ClientService } from "../services/client.service";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  addUser: FormGroup;
  isSubmit=false;
  managers=[];
  constructor(private fb: FormBuilder, public clientService : ClientService) { }

  ngOnInit() {
    this.addUser = this.fb.group({
      name:  [null, Validators.compose([Validators.required])],
      email:  [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])],
      manager: [null, Validators.compose([Validators.required])],
      role: [null, Validators.compose([Validators.required])]
    });
    this.allManager();
    this.changeValidation("");
  }


  allManager(){
    this.clientService.allManager().subscribe((response:any)=>{
      //console.log(response);
      if(response.status){
        this.managers = response.data;
      }
    })
  }
  registerSubmit(){
    this.isSubmit=true;
    let manager;
    if(this.addUser.valid){
      let datetime = new Date();
      if(this.addUser.controls.role.value==="user"){
        manager = this.addUser.controls.manager.value;
      }else{
        manager = null;
      }
      let formData = {  
                      name:this.addUser.controls.name.value, 
                      email:this.addUser.controls.email.value,
                      password:this.addUser.controls.password.value, 
                      role:this.addUser.controls.role.value,
                      manager:manager, 
                      createddate:datetime,
                      updateddate :datetime
      }
      this.clientService.addUser(formData).subscribe((response:any)=>{
        //console.log(response);
        this.addUser.reset();
        this.isSubmit=false;
        if(response.status){
          alert('User added..');
        }
      })
    }
  }

  changeValidation(role){
    const managerControle = this.addUser.get('manager');
    if(role!=='user'){
      managerControle.clearValidators();
      managerControle.disable();
    }else{
      managerControle.setValidators([Validators.required]);
      managerControle.enable();
    }
    managerControle.updateValueAndValidity();
  }
}
