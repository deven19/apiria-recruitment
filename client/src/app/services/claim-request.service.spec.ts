import { TestBed } from '@angular/core/testing';

import { ClaimRequestService } from './claim-request.service';

describe('ClaimRequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClaimRequestService = TestBed.get(ClaimRequestService);
    expect(service).toBeTruthy();
  });
});
