import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http : HttpClient, public router: Router) {
    
   }

  getUsers(){
    let token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + token);
    let httpOptions = {
      headers: headers_object
    };
    return this.http.get('http://localhost:1520/api/user/getusers/', httpOptions)
  }

  allManager(){
    let token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + token);
    let httpOptions = {
      headers: headers_object
    };
    return this.http.get('http://localhost:1520/api/user/allManager/',httpOptions)
  }

  addUser(data){
    let token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + token);
    let httpOptions = {
      headers: headers_object
    };
    return this.http.post('http://localhost:1520/api/user/addUser/', data, httpOptions)
  }
}
