import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http : HttpClient, public router: Router) { }

  login(data:any){
    let result;
    this.http.post('http://localhost:1520/api/auth/login/',data).subscribe(res => {
      result = res;
      if(result.status===1){
        sessionStorage.setItem('loggedInUser',JSON.stringify(result));
        let loggedInUser = JSON.parse(sessionStorage.getItem('loggedInUser'));
        if(result.data.role==='admin' || result.data.role==='manager'){
          this.router.navigate(['../pending-claim'], {});
        }else{
          this.router.navigate(['../my-claims'], {});
        }
        
      }else if(result.status===0){
        //console.log(result);
        alert(result.message);
      }else if(result.status===2){
        //console.log(result);
        alert(result.message);
      }
    });
  }

  signUp(data:any){
    let result;
    this.http.post('http://localhost:1520/api/auth/signup/',data).subscribe(res => {
      result = res;
      if(result.status===1){
        //console.log('user created successfuly please login.. with same credential...');
        alert('user created..');
        this.router.navigate(['../login'], {});
      }else if(result.status===0){
        alert(result.message);
        //console.log(result);
      }else if(result.status===2){
        //console.log(result);
        alert(result.message);
      }
    });
  }
}
