import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class ClaimRequestService {

  constructor(private http : HttpClient, public router: Router) { }

  claimRequest(data){
    let token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + token);
    let httpOptions = {
      headers: headers_object
    };
    return this.http.post('http://localhost:1520/api/claim/claimRequest/',data, httpOptions)
  }

  getClaim(){
    let token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + token);
    let httpOptions = {
      headers: headers_object
    };
    return this.http.get('http://localhost:1520/api/claim/getClaim/'+JSON.parse(sessionStorage.getItem('loggedInUser')).data.email+'/'+JSON.parse(sessionStorage.getItem('loggedInUser')).data.role,httpOptions);
  }

  getMyClaim(){
    let token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + token);
    let httpOptions = {
      headers: headers_object
    };
    return this.http.get('http://localhost:1520/api/claim/getMyClaim/'+JSON.parse(sessionStorage.getItem('loggedInUser')).data.email,httpOptions);
  }

  getPendingClaim(){
    let token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + token);
    let httpOptions = {
      headers: headers_object
    };
    return this.http.get('http://localhost:1520/api/claim/getPendingClaim/'+JSON.parse(sessionStorage.getItem('loggedInUser')).data.email+'/'+JSON.parse(sessionStorage.getItem('loggedInUser')).data.role, httpOptions);
  }
  
  claimAction(data){
    let token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + token);
    let httpOptions = {
      headers: headers_object
    };
     return this.http.put('http://localhost:1520/api/claim/claimAction/',data, httpOptions);
  }
  
}
