import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClickButton.ParentComponent } from './click-button.parent.component';

describe('ClickButton.ParentComponent', () => {
  let component: ClickButton.ParentComponent;
  let fixture: ComponentFixture<ClickButton.ParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClickButton.ParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClickButton.ParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
