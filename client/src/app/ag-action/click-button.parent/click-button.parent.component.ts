import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {ICellRendererAngularComp} from "ag-grid-angular";
import {ClaimRequestService } from "../../services/claim-request.service";

@Component({
  selector: 'app-click-button.parent',
  templateUrl: './click-button.parent.component.html',
  styleUrls: ['./click-button.parent.component.css']
})
export class ClickButtonParentComponent implements ICellRendererAngularComp {
  private params: any;
  public cell: any;
  //@Input() responseData: any;
  @Output() responseData = new EventEmitter<boolean>();

  constructor(public claimRequestService : ClaimRequestService) {

  }

  agInit(params: any): void {
      this.params = params;
      this.cell = {row: params.value, col: params.colDef.headerName, other: params.row};
  }

  public clicked(cell: any): void {
    //console.log(this.params.data )
    //console.log("Child Cell Clicked: " + JSON.stringify(cell));
    let data = {_id: this.params.data._id, status:cell.actions, updateddate:new Date()}
    this.claimRequestService.claimAction(data).subscribe((res:any)=>{
      let response = res;
      //console.log(response);
      this.responseData.emit(response);
      //this.rowData= response.data;
    });
  }

  refresh(): boolean {
      return false;
  }
}