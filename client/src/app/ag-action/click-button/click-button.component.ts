import { Component, OnInit,  EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-click-button',
  templateUrl: './click-button.component.html',
  styleUrls: ['./click-button.component.css']
})
export class ClickButtonComponent implements OnInit {
  @Input() cell: any;
  @Input() responseData: any;
  @Output() onClicked = new EventEmitter<boolean>();

  click(action): void {
    this.cell.actions =action 
    this.onClicked.emit(this.cell);
  }
  constructor() { }

  ngOnInit() {
  }

  ngOnChange(){
   // console.log({"this.responseData":this.responseData});
  }
}





