import { Component, OnInit } from '@angular/core';
import {GridOptions, GridApi} from "ag-grid-community";
import { ClickButtonParentComponent } from "../ag-action/click-button.parent/click-button.parent.component"
import { ClaimRequestService } from "../services/claim-request.service"

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  public api: GridApi;
  public rowData = [];
  private gridOptions: GridOptions;
//https://www.ag-grid.com/angular-getting-started/
    constructor(public claimRequestService : ClaimRequestService) {
        this.gridOptions = <GridOptions>{};
        this.gridOptions.columnDefs = [
            {
              headerName: "User",
              field: "user",
              width: 180,
              sortable: true,
              filter: true
            },
            {
              headerName: "Reimbursement Type",
              field: "type",
              width: 150,
              sortable: true,
              filter: true
            },
            {
              headerName: "Month",
              field: "month",
              width: 100,
              sortable: true,
              filter: true
            },
            {
              headerName: "Amount",
              field: "amount",
              width: 100,
              sortable: true,
              filter: true
            },
            {
              headerName: "Status",
              field: "status",
              width: 100,
              sortable: true,
              filter: true
            },
            {
              headerName: "Created Date",
              field: "createddate",
              width: 180,
              sortable: true,
              filter: true
            },
            {
              headerName: "Updated Date",
              field: "updateddate",
              width: 180,
              sortable: true,
              filter: true
            },
            
            // {
            //     headerName: "Action",
            //     field: "action",
            //     width: 100,
            //     sortable: true,
            //     filter: true,
            //     cellRendererFramework:ClickButtonParentComponent
            //     // cellRenderer : (params)=>{
            //     //   console.log(params.data._id);
            //     //   if(params.data.status==='pending'){
            //     //     return '<button onclick="click()" class="btn" title="Approve"><i class="fa fa-check green"></i></button>';
            //     //   }

            //     // }
            // },


        //     {
        //       headerName: "action",
        //       field: "action",
        //       width: 50,
        //       sortable: true,
        //       filter: true,
        //       onCellClicked:(params)=>{
        //         this.clickCell("approve",params.data);
        //       },
        //       cellRenderer : (params)=>{
        //         console.log(params.data._id);
        //         if(params.data.status!=='pending'){
        //           return '<button class="btn" title="Approve"><i class="fa fa-check green"></i></button>';
        //         }

        //       }
        //   },



        //   {
        //     headerName: "action",
        //     field: "action",
        //     width: 50,
        //     sortable: true,
        //     filter: true,
        //     onCellClicked:(params)=>{
        //       this.clickCell("reject",params.data);
        //     },
        //     cellRenderer : (params)=>{
        //       console.log(params.data._id);
        //       if(params.data.status!=='pending'){
        //         return '<button class="btn" title="Approve"><i class="fa fa-close red"></i></button>';
        //       }
        //     }
        // },
        ];
    }

  ngOnInit() {
    this.getClaim();
  }

  onGridReady(params): void {
    
  }


getClaim(){
  this.claimRequestService.getClaim().subscribe((res:any)=>{
    let response = res;
    //console.log(response);
    this.rowData= response.data;
  });
}

  clickCell(action,data){
    if(data && (data.status =='approve' || data.status =='reject')){
      data = {_id: data._id, status:action,updateddate:new Date()}
      //console.log(data)
      this.claimRequestService.claimAction(data).subscribe((res:any)=>{
        let response = res;
        //console.log(response);
        this.getClaim();
      });
    }
  }
}
