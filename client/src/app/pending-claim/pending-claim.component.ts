import { Component, OnInit } from '@angular/core';
import {GridOptions, GridApi} from "ag-grid-community";
import { ClaimRequestService } from "../services/claim-request.service";
import { DatePipeComponent } from "../ag-action/date-pipe/date-pipe.component";

@Component({
  selector: 'app-pending-claim',
  templateUrl: './pending-claim.component.html',
  styleUrls: ['./pending-claim.component.css']
})
export class PendingClaimComponent implements OnInit {
  public api: GridApi;
  public rowData = [];
  private gridOptions: GridOptions;
//https://www.ag-grid.com/angular-getting-started/
    constructor(public claimRequestService : ClaimRequestService) {
        this.gridOptions = <GridOptions>{};
        this.gridOptions.columnDefs = [
            {
              headerName: "User",
              field: "user",
              width: 150,
              sortable: true,
              filter: true
            },
            {
              headerName: "Type",
              field: "type",
              width: 100,
              sortable: true,
              filter: true
            },
            {
              headerName: "Month",
              field: "month",
              width: 100,
              sortable: true,
              filter: true
            },
            {
              headerName: "Amount",
              field: "amount",
              width: 100,
              sortable: true,
              filter: true
            },
            {
              headerName: "Status",
              field: "status",
              width: 100,
              sortable: true,
              filter: true
            },
            {
              headerName: "Created Date",
              field: "createddate",
              width: 180,
              sortable: true,
              filter: true,
              cellEditorFramework : DatePipeComponent
            },
            {
              headerName: "Updated Date",
              field: "updateddate",
              width: 180,
              sortable: true,
              filter: true
            },
            {
              headerName: "",
              field: "action",
              width: 35,
              resizable:false,
              cellClass:['ag-cell-custom'],
              onCellClicked:(params)=>{
                this.clickCell("approve",params.data);
              },
              cellRenderer : (params)=>{
                //console.log(params.data._id);
                if(params.data.status=='pending'){
                  return '<button class="btn btn-action" title="Approve"><i class="fa fa-check green"></i></button>';
                }

              }
          },



          {
            headerName: "",
            field: "action",
            width: 35,
            cellClass:['ag-cell-custom'],
            onCellClicked:(params)=>{
              this.clickCell("reject",params.data);
            },
            cellRenderer : (params)=>{
              //console.log(params.data._id);
              if(params.data.status=='pending'){
                return '<button class="btn btn-action" title="Approve"><i class="fa fa-close red"></i></button>';
              }
            }
        },
        ];
    }

  ngOnInit() {
    this.getPendingClaim();
  }

  onGridReady(params): void {
    
  }


getPendingClaim(){
  this.claimRequestService.getPendingClaim().subscribe((res:any)=>{
    let response = res;
    //console.log(response);
    this.rowData= response.data;
  });
}

  clickCell(action,data){
    if(data && (data.status =='pending')){
      data = {_id: data._id, status:action,updateddate:new Date()}
      //console.log(data)
      this.claimRequestService.claimAction(data).subscribe((res:any)=>{
        let response = res;
        //console.log(response);
        this.getPendingClaim();
      });
    }
  }
}
