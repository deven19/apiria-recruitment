import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  loggedInUser:any
  userRole;
  constructor(public router: Router) {
    this.loggedInUser = JSON.parse(sessionStorage.getItem("loggedInUser"));
    //console.log(this.loggedInUser);
    this.userRole = this.loggedInUser.data.role;
    //console.log({"role":this.userRole})
  }

  ngOnInit() {
  }

  logout(){
    sessionStorage.clear();
    this.router.navigate(['../'], {});
  }
}
