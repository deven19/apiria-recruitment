import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './guard/auth.guard';

import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { ReimbursementFormComponent } from './reimbursement-form/reimbursement-form.component';
import { AllUserComponent } from './all-user/all-user.component';
import { AddUserComponent } from './add-user/add-user.component';
import { PendingClaimComponent } from './pending-claim/pending-claim.component';
import { MyClaimComponent } from './my-claim/my-claim.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login', pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'sign-up',
    component: SignupComponent
  },
  {
    path: 'all-claim',
    component: HomeComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'pending-claim',
    component: PendingClaimComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'my-claims',
    component: MyClaimComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'make-claim',
    component: ReimbursementFormComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'users-list',
    component: AllUserComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'add-user',
    component: AddUserComponent,
    canActivate : [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
