import { Component, OnInit } from '@angular/core';
import {GridOptions, GridApi} from "ag-grid-community";
import { ClickButtonParentComponent } from "../ag-action/click-button.parent/click-button.parent.component"

import { ClientService } from '../services/client.service';

@Component({
  selector: 'app-all-user',
  templateUrl: './all-user.component.html',
  styleUrls: ['./all-user.component.css']
})
export class AllUserComponent implements OnInit {

  public api: GridApi;
  public rowData = [];
  private gridOptions: GridOptions;
    constructor(private clientService:ClientService) {
        this.gridOptions = <GridOptions>{};
        this.gridOptions.columnDefs = [
            {
                headerName: "Name",
                field: "name",
                width: 150,
                sortable: true,
                filter: true
            },
            {
              headerName: "Email ID",
              field: "email",
              width: 150,
              sortable: true,
              filter: true
            },
            {
              headerName: "Role",
              field: "role",
              width: 150,
              sortable: true,
              filter: true
            },
            {
              headerName: "Manager",
              field: "manager",
              width: 150,
              sortable: true,
              filter: true
            }

        ];
        // this.gridOptions.rowData = [
        //     // {name: "5" , email: "test1", manager:null},
        //     // {name: "10", email: "test1", manager:"Anull"},
        //     // {name: "15", email: "test1", manager:null}
        // ];
        
    }

  ngOnInit() {
    this.clientService.getUsers().subscribe((res:any)=>{
      let response = res;
      //console.log(response);
      this.rowData= response.data;
    });
  }

  onGridReady(params): void {
    
  }
}
