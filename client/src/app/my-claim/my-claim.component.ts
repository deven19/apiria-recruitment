import { Component, OnInit } from '@angular/core';
import {GridOptions, GridApi} from "ag-grid-community";
import { ClaimRequestService } from "../services/claim-request.service"

@Component({
  selector: 'app-my-claim',
  templateUrl: './my-claim.component.html',
  styleUrls: ['./my-claim.component.css']
})
export class MyClaimComponent implements OnInit {
  public api: GridApi;
  public rowData = [];
  private gridOptions: GridOptions;
//https://www.ag-grid.com/angular-getting-started/
    constructor(public claimRequestService : ClaimRequestService) {
        this.gridOptions = <GridOptions>{};
        this.gridOptions.columnDefs = [
            {
              headerName: "Reimbursement Type",
              field: "type",
              width: 150,
              sortable: true,
              filter: true
            },
            {
              headerName: "Month",
              field: "month",
              width: 100,
              sortable: true,
              filter: true
            },
            {
              headerName: "Amount",
              field: "amount",
              width: 100,
              sortable: true,
              filter: true
            },
            {
              headerName: "Status",
              field: "status",
              width: 100,
              sortable: true,
              filter: true
            },
            {
              headerName: "Created Date",
              field: "createddate",
              width: 180,
              sortable: true,
              filter: true
            },
            {
              headerName: "Updated Date",
              field: "updateddate",
              width: 180,
              sortable: true,
              filter: true
            },


            {
              headerName: "",
              field: "action",
              width: 35,
              cellClass:['ag-cell-custom'],
              onCellClicked:(params)=>{
                this.clickCell("remove",params.data);
              },
              cellRenderer : (params)=>{
                //console.log(params.data._id);
                if(params.data.status=='pending'){
                  return '<button class="btn btn-action" title="Remove"><i class="fa fa-trash green"></i></button>';
                }

              }
          }
        ];
    }

  ngOnInit() {
    this.getMyClaim();
  }

  onGridReady(params): void {
    
  }


  getMyClaim(){
  this.claimRequestService.getMyClaim().subscribe((res:any)=>{
    let response = res;
    //console.log(response);
    this.rowData= response.data;
  });
}

  clickCell(action,data){
    if(data && (data.status =='pending')){
      //console.log(data);
      data = {_id: data._id, status:action,updateddate:new Date()}
      //console.log(data)
      this.claimRequestService.claimAction(data).subscribe((res:any)=>{
        let response = res;
        //console.log(response);
        this.getMyClaim();
      });
    }
  }
}
