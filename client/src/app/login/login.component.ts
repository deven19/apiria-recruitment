import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormArray, FormControl, NgForm, NgModel } from "@angular/forms";

import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login: FormGroup;
  isSubmit=false;
  constructor(private fb: FormBuilder,public auth:AuthenticationService) { 
    // let data = {"email":"bob@gmail.com","password":"111111"}
    // this.auth.login(data);
  }

  ngOnInit() {
    this.login = this.fb.group({
      password:  [null, Validators.compose([Validators.required])],
      email:  [null, Validators.compose([Validators.required])]
    });
  }

  loginSubmit(){
    //console.log(this.login);
    this.isSubmit=true;
    if(this.login.valid){
    let data = {"email":this.login.controls.email.value,"password":this.login.controls.password.value}
    this.auth.login(data);
    }
  }
}
