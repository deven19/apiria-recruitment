import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormArray, FormControl, NgForm, NgModel } from "@angular/forms";
import { AuthenticationService } from '../services/authentication.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signup: FormGroup;
  isSubmit=false;
  confirmPassErr=false;
  constructor(private fb: FormBuilder, public auth:AuthenticationService) { }
  ngOnInit() {
    this.signup = this.fb.group({
      password:  [null, Validators.compose([Validators.required])],
      repassword:  [null, Validators.compose([Validators.required])],
      // role:  [null, Validators.compose([Validators.required])],
      term:  [null, Validators.compose([Validators.required])],
      name:  [null, Validators.compose([Validators.required])],
      email:  [null, Validators.compose([Validators.required])]
    });
  }

  signupSubmit(){
    this.isSubmit=true;
    if(this.signup.valid){
      if(this.checkPasswords(this.signup)){
        this.confirmPassErr = false;
        let datetime = new Date();
        let data = {
                    "name":this.signup.controls.name.value,
                    "email":this.signup.controls.email.value,
                    "role":"admin",
                    "password":this.signup.controls.password.value,
                    createddate:datetime,
                    updateddate:datetime}
        this.auth.signUp(data);
      }else{
        this.confirmPassErr = true; 
      }
    }
  }

  checkPasswords(data:any) { // here we have the 'passwords' group
  let pass = data.controls.password.value;
  let confirmPass = data.controls.repassword.value;
  return pass === confirmPass ? true : false;      
}
}
