import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FileSelectDirective} from "ng2-file-upload";

import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from './guard/auth.guard';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { ClickButtonComponent } from './ag-action/click-button/click-button.component';
import { ClickButtonParentComponent } from './ag-action/click-button.parent/click-button.parent.component';
import { NavComponent } from './nav/nav.component';
import { ReimbursementFormComponent } from './reimbursement-form/reimbursement-form.component';
import { AddUserComponent } from './add-user/add-user.component';
import { AllUserComponent } from './all-user/all-user.component';
import { PendingClaimComponent } from './pending-claim/pending-claim.component';
import { MyClaimComponent } from './my-claim/my-claim.component';
import { DatePipeComponent } from './ag-action/date-pipe/date-pipe.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    HomeComponent,
    ClickButtonComponent,
    ClickButtonParentComponent,
    NavComponent,
    ReimbursementFormComponent,
    FileSelectDirective,
    AddUserComponent,
    AllUserComponent,
    PendingClaimComponent,
    MyClaimComponent,
    DatePipeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AgGridModule.withComponents([ClickButtonParentComponent, DatePipeComponent]),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule 
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
