const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('./../config.json');
var q = require('q');
const saltRounds = 10;

var services = {};

services.login = login;
services.signUp = signUp;
services.getusers = getusers;
services.addUser = addUser;
services.allManager = allManager;
module.exports = services;


function login(req){
    var defer = q.defer();
    let db = global.dbconnections;
    let data = req.body;
    let search = {email: data.email.toLowerCase()};
    db.collection('users', (err, collection) => {
        collection.find(search).toArray((err, result) => {
            if (err){
                defer.resolve({status : 10, message:"something went wrong please try again after sometime.."});
                console.log(err);
            };

            if(result.length>0){
                if(bcrypt.compareSync(data.password, result[0].password)){
                    delete result[0]._id;
                    delete result[0].password;
                    var token = jwt.sign({ data: result[0] }, config.jwt_secret);
                    defer.resolve({status : 1, message:"login success", data:result[0], token:token});
                }else{
                    defer.resolve({status : 2, message:"invalid pass"});
                }
            }else{
                defer.resolve({status : 0, message:"no user"});
            }
        });

    });
    return defer.promise
}

function signUp(req){
    var defer = q.defer();
    var db = global.dbconnections;
    var hash = bcrypt.hashSync(req.body.password, saltRounds);
    let data = {name: req.body.name,email: req.body.email.toLowerCase(),password: hash,role:req.body.role};
    let search = {email: data.email};

    db.collection('users', (err, collection) => {
        collection.find(search).toArray((err, result) => {
            if (err){
                defer.resolve({status : 10, message:"something went wrong please try again after sometime.."});
            };

            if(result.length>0){
                defer.resolve({status : 2, message:"user already..."});
            }else{
                db.collection("users").insertOne(data, (err, result) => {  
                    if (err){
                        defer.resolve({status : 10, message:"something went wrong please try again after sometime.."});
                    };
                    
                    if(result.insertedCount){
                        defer.resolve({status : 1, data : result.ops, message:"Inserted.."});
                    }
                }); 
            }
        });

    });
    return defer.promise;
}


function getusers(){
    var defer = q.defer();
    var db = global.dbconnections;
    //let search = {role: "manager"};

    db.collection('users', (err, collection) => {
        collection.find().toArray((err, result) => {
            if (err){
                defer.resolve({status : 10, message:"something went wrong please try again after sometime.."});
            };

            if(result.length>0){
                defer.resolve({status : 1, data:result});
            }else{
                defer.resolve({status : 1, data:[]});
            }
        });

    });
    return defer.promise
}

function allManager(){
    var defer = q.defer();
    var db = global.dbconnections;
    let search = {role:"manager"};

    db.collection('users', (err, collection) => {
        collection.find(search).toArray((err, result) => {
            if (err){
                defer.resolve({status : 10, message:"something went wrong please try again after sometime.."});
            };

            if(result.length>0){
                defer.resolve({status : 1, data:result});
            }
        });

    });
    return defer.promise
}

function addUser(req){
    var defer = q.defer();
    var db = global.dbconnections;
    var hash = bcrypt.hashSync(req.body.password, saltRounds);
    let data = {name: req.body.name,email: req.body.email.toLowerCase(),password: hash,role:req.body.role, 
        manager:req.body.manager, 
        createddate:req.body.createddate,
        updateddate :req.body.updateddate
    };
    let search = {email: data.email};

    db.collection('users', (err, collection) => {
        collection.find(search).toArray((err, result) => {
            if (err){
                defer.resolve({status : 10, message:"something went wrong please try again after sometime.."});
            };

            if(result.length>0){
                defer.resolve({status : 2, message:"user already..."});
            }else{
                db.collection("users").insertOne(data, (err, result) => {  
                    if (err){
                        defer.resolve({status : 10, message:"something went wrong please try again after sometime.."});
                    };
                    
                    if(result.insertedCount){
                        defer.resolve({status : 1, data : result.ops, message:"Inserted.."});
                    }
                }); 
            }
        });

    });
    return defer.promise;
}