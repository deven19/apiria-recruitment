var services = {};
services.connectMongoDB = connectMongoDB;
module.exports = services;

function connectMongoDB(){
    mongodb.MongoClient.connect(config.mongodb.url, { useNewUrlParser: true }, function(err, client) {
        if (err) {
            console.log({ result: 'connection error...', log: { error_name: err.name, error_message: err.message } });
        }
    
        if (client) {
            console.log("Connected to mongodb server");
            var db = client.db(config.mongodb.database);
            global.dbconnections = db;
    
        }
    })
    
}