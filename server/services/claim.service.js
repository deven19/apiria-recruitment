const config = require('./../config.json');
var q = require('q');
var fs = require('fs');
var ObjectId = require('mongodb').ObjectID;
var services = {};

services.claimRequest = claimRequest;
services.getClaim = getClaim;
services.claimAction = claimAction;
services.getMyClaim = getMyClaim;
services.getPendingClaim = getPendingClaim;
module.exports = services;



function claimRequest(req, res){
    var defer = q.defer();
    let db = global.dbconnections;
    let data = req.body;
    let search = {email: data.email};

    let docs = req.body.docs;
    if(docs.length>0){
        for (var id in docs) {
            var file = docs[id].base64.split(';base64,').pop();
            var bitmap = new Buffer.from(file, 'base64');
            //console.log({name:docs[id].name, id:id});
            fs.writeFileSync("uploads/"+docs[id].name, bitmap,  {encoding: 'base64'});
        }

        delete req.body.docs;
        db.collection("claim").insertOne(req.body, (err, result) => {  
            if (err){
                defer.resolve({status : 10, message:"something went wrong please try again after sometime.."});
            };
            
            if(result.insertedCount){
                //console.log({status : 1, data : result.ops, message:"Inserted.."});
                defer.resolve({status : 1, data : result.ops, message:"Inserted.."})
            }
        });
            
    }else{
        defer.resolve({"status": 2, "message":"file not found"});
    }
    return defer.promise
}


function getClaim(req){
    var defer = q.defer();
    var db = global.dbconnections;

    let role = req.params.role;
    let search;
    if(role==="admin"){
        search = {email: {'$ne':req.params.email}};
    }else if(role==="manager"){
        search = {$and: [{user: {'$ne':req.params.email}}, {manager:req.params.email}]};
    }
    db.collection('claim', (err, collection) => {
        collection.find(search).toArray((err, result) => {
            if (err){
                defer.resolve({status : 10, message:"something went wrong please try again after sometime.."});
            };

            if(result.length>0){
                defer.resolve({status : 1, data:result});
            }else if(result.length===0){
                defer.resolve({status : 1, data:result, message:"no data found"});
            }
        });

    });
    return defer.promise
}



function getMyClaim(req){
    var defer = q.defer();
    var db = global.dbconnections;
    let search = {user: {'$eq':req.params.email}};

    db.collection('claim', (err, collection) => {
        collection.find(search).toArray((err, result) => {
            if (err){
                defer.resolve({status : 10, message:"something went wrong please try again after sometime.."});
            };

            if(result.length>0){
                defer.resolve({status : 1, data:result});
            }else if(result.length===0){
                defer.resolve({status : 1, data:result, message:"no data found"});
            }
        });

    });
    return defer.promise
}



function getPendingClaim(req){
    var defer = q.defer();
    var db = global.dbconnections;
    let role = req.params.role;
    let search;
    if(role==="admin"){
        search = {$and: [{user: {'$ne':req.params.email}}, {status:'pending'}]};
    }else if(role==="manager"){
        search = {$and: [{user: {'$ne':req.params.email}}, {status:'pending'}, {manager:req.params.email}]};
    }

    db.collection('claim', (err, collection) => {
        collection.find(search).toArray((err, result) => {
            if (err){
                defer.resolve({status : 10, message:"something went wrong please try again after sometime.."});
            };
            if(result.length>0){
                defer.resolve({status : 1, data:result});
            }else if(result.length===0){
                defer.resolve({status : 1, data:result, message:"no data found"});
            }
        });

    });
    return defer.promise
}

function claimAction(req){
    var defer = q.defer();
    var db = global.dbconnections;

    var myquery = {"_id": ObjectId(req.body._id)  };

    if(req.body.status==='remove'){
        db.collection('claim', (err, collection) => {
            collection.deleteOne(myquery, (err, result) => {
                if (err){
                    console.log(err);
                    defer.resolve({status : 10, message:"something went wrong please try again after sometime.."});
                };
    
                if(result.result.n>0){
                    defer.resolve({status : 1, message:result.n+"document deleted"});
                }
            });
    
        });
    }else{
        var newvalues = { $set: {status: req.body.status, updateddate:req.body.updateddate} };
        db.collection('claim', (err, collection) => {
            collection.updateOne(myquery, newvalues, (err, result) => {
                if (err){
                    console.log(err);
                    defer.resolve({status : 10, message:"something went wrong please try again after sometime.."});
                };
    
                if(result.modifiedCount>0){
                    defer.resolve({status : 1, message:"record updated"});
                }
            });
    
        });
    }
    return defer.promise
}


