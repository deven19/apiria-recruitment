(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/add-user/add-user.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/add-user/add-user.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"wrapper\">\n        <app-nav></app-nav>\n        <div id=\"page-wrapper\" class=\"gray-bg dashbard-1\">\n            <div class=\"content-main\">\n                <div class=\"banner\">\n                    <h2>\n                        <a href=\"index.html\">Home</a>\n                        <i class=\"fa fa-angle-right\"></i>\n                        <span>Add User</span>\n                    </h2>\n                </div>\n                <div class=\"blank\">\n                    <div class=\"blank-page\">\n                        <div class=\"grid-form1\">\n                            <div class=\"panel-body\">\n                                <form role=\"form\" class=\"form-horizontal\" [formGroup]=\"addUser\"\n                                    (ngSubmit)=\"registerSubmit()\">\n\n                                    <div class=\"form-group\">\n                                        <label class=\"col-md-3 control-label\"> Name*</label>\n                                        <div class=\"col-md-7\">\n                                            <div class=\"input-group\">\n                                                <input type=\"text\" formControlName=\"name\" class=\"form-control1\">\n                                            </div>\n                                        </div>\n                                        <div class=\"col-sm-2\">\n                                            <p class=\"help-block\" *ngIf=\"(addUser.get('name').hasError('required') && (addUser.get('name').dirty || addUser.get('name').touched)) || (addUser.get('name').hasError('required') && !addUser.get('name').valid && isSubmit)\">Name Required</p>\n                                        </div>\n                                    </div>\n\n                                    <div class=\"form-group\">\n                                        <label class=\"col-md-3 control-label\"> Email*</label>\n                                        <div class=\"col-md-7\">\n                                            <div class=\"input-group\">\n                                                <input type=\"text\" formControlName=\"email\" class=\"form-control1\">\n                                            </div>\n                                        </div>\n                                        <div class=\"col-sm-2\">\n                                            <p class=\"help-block\" *ngIf=\"(addUser.get('email').hasError('required') && (addUser.get('email').dirty || addUser.get('email').touched)) || (addUser.get('email').hasError('required') && !addUser.get('email').valid && isSubmit)\">Email Required</p>\n                                        </div>\n                                    </div>\n\n                                    <div class=\"form-group\">\n                                        <label class=\"col-md-3 control-label\"> Password* </label>\n                                        <div class=\"col-md-7\">\n                                            <div class=\"input-group\">\n                                                <input type=\"password\" formControlName=\"password\" value=\"1234\" class=\"form-control1\">\n                                            </div>\n                                        </div>\n                                        <div class=\"col-sm-2\" >\n                                            <p class=\"help-block\" *ngIf=\"(addUser.get('password').hasError('required') && (addUser.get('password').dirty || addUser.get('password').touched)) || (addUser.get('password').hasError('required') && !addUser.get('password').valid && isSubmit)\">Password Required</p>\n                                        </div>\n                                    </div>\n\n                                    <div class=\"form-group\">\n                                        <label class=\"col-md-3 control-label\">Select Role* </label>\n                                        <div class=\"col-md-7\">\n                                            <div class=\"input-group\">\n                                                <select class=\"form-control1\" (change)=\"changeValidation($event.target.value);\" formControlName=\"role\">\n                                                    <option value=\"admin\">Admin</option>\n                                                    <option value=\"manager\">Manager</option>\n                                                    <option value=\"user\">User</option>\n                                                </select>\n                                                <!-- <input type=\"checkbox\" formControlName=\"AdminRole\" value=\"admin\"> Admin \n                                                <input type=\"checkbox\" formControlName=\"managerRole\" value=\"manager\"> Manager \n                                                <input type=\"checkbox\" formControlName=\"userRole\" value=\"user\"> User  -->\n                                            </div>\n                                        </div>\n                                        <div class=\"col-sm-2\">\n                                            <p class=\"help-block\" *ngIf=\"(addUser.get('role').hasError('required') && (addUser.get('role').dirty || addUser.get('role').touched)) || (addUser.get('role').hasError('required') && !addUser.get('role').valid && isSubmit)\">Please select a role</p>\n                                        </div>\n                                    </div>\n                                    <div class=\"form-group\">\n                                        <label class=\"col-md-3 control-label\">Select Manager* </label>\n                                        <div class=\"col-md-7\">\n                                            <div class=\"input-group\">\n                                                <select class=\"form-control1\" (change)=\"changeValidation($event.target.value);\" formControlName=\"manager\">\n                                                    <option *ngFor=\"let manager of managers\" value=\"{{manager.email}}\">{{manager.name}} {{manager.email}}</option>\n                                                </select>\n                                            </div>\n                                        </div>\n                                        <div class=\"col-sm-2\">\n                                            <p class=\"help-block\" *ngIf=\"(addUser.get('manager').hasError('required') && (addUser.get('manager').dirty || addUser.get('manager').touched)) || (addUser.get('manager').hasError('required') && !addUser.get('manager').valid && isSubmit)\">Manager name Required</p>\n                                        </div>\n                                    </div>\n                                    <div class=\"panel-footer\">\n                                        <div class=\"row\">\n                                            <div class=\"col-sm-8 col-sm-offset-2\">\n                                                <button type=\"submit\" class=\"btn-primary btn\">Submit</button>\n                                                <button class=\"btn-default btn\">Cancel</button>\n                                                <button class=\"btn-inverse btn\">Reset</button>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </form>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"copy\">\n                    <p> &copy; 2019 Minimal. All Rights Reserved </p>\n                </div>\n            </div>\n        </div>\n        <div class=\"clearfix\"> </div>\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/ag-action/click-button.parent/click-button.parent.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/ag-action/click-button.parent/click-button.parent.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-click-button (onClicked)=\"clicked($event)\" [cell]=\"cell\"></app-click-button>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/ag-action/click-button/click-button.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/ag-action/click-button/click-button.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <button style=\"height: 21px\"  class=\"btn btn-info\"></button> -->\n<button (click)=\"click('approve')\" class=\"btn\" title=\"Approve\"><i class=\"fa fa-check green\"></i></button>\n<button (click)=\"click('reject')\" class=\"btn\" title=\"Reject\"><i class=\"fa fa-close red\"></i></button>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/ag-action/date-pipe/date-pipe.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/ag-action/date-pipe/date-pipe.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "{{params.value  | date: 'dd/MM/yyyy'}}"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/all-user/all-user.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/all-user/all-user.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"wrapper\">\n        <app-nav></app-nav>\n        <div id=\"page-wrapper\" class=\"gray-bg dashbard-1\">\n            <div class=\"content-main\">\n                <div class=\"banner\">\n                    <h2>\n                        <a href=\"index.html\">Home</a>\n                        <i class=\"fa fa-angle-right\"></i>\n                        <span>All Users</span>\n                    </h2>\n                </div>\n                <div class=\"blank\">\n                    <div class=\"blank-page\">\n                        <ag-grid-angular #agGrid style=\"width: 100%; height: 350px;\" class=\"ag-theme-balham\"\n                            [gridOptions]=\"gridOptions\" resizable =\"true\" [pagination]=\"true\" [paginationPageSize]=\"10\" (gridReady)=\"onGridReady($event)\" [rowData]=\"rowData\">\n                        </ag-grid-angular>\n                    </div>\n                </div>\n                <div class=\"copy\">\n                    <p> &copy; 2019 Minimal. All Rights Reserved </p>\n                </div>\n            </div>\n        </div>\n        <div class=\"clearfix\"> </div>\n    </div>\n    <router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"wrapper\">\n    <app-nav></app-nav>\n    <div id=\"page-wrapper\" class=\"gray-bg dashbard-1\">\n        <div class=\"content-main\">\n            <div class=\"banner\">\n                <h2>\n                    <a href=\"index.html\">Home</a>\n                    <i class=\"fa fa-angle-right\"></i>\n                    <span>All Claim</span>\n                </h2>\n            </div>\n            <div class=\"blank\">\n                <div class=\"blank-page\">\n                    <ag-grid-angular #agGrid style=\"width: 100%; height: 350px;\" class=\"ag-theme-balham\"\n                    [gridOptions]=\"gridOptions\" [pagination]=\"true\" [paginationPageSize]=\"10\" (gridReady)=\"onGridReady($event)\" [rowData]=\"rowData\">\n                    </ag-grid-angular>\n                </div>\n                <!-- Button to Open the Modal -->\n            </div>\n            <div class=\"copy\">\n                <p> &copy; 2019 Minimal. All Rights Reserved </p>\n            </div>\n        </div>\n    </div>\n    <div class=\"clearfix\"> </div>\n</div>\n\n\n      \n      \n<router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/login/login.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login/login.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login\">\n    <h1><a [attr.href]=\"\">Apiria </a></h1>\n    <div class=\"login-bottom\">\n        <h2>Login</h2>\n        <form [formGroup]=\"login\" (ngSubmit)=\"loginSubmit()\">\n        <div class=\"col-md-6\">\n            <div class=\"error-label\">\n                <label class=\"error\" *ngIf=\"(login.get('email').hasError('required') && (login.get('email').dirty || login.get('email').touched)) || (login.get('email').hasError('required') && !login.get('email').valid && isSubmit)\">Please enter email</label>\n            </div>\n            <div class=\"login-mail\">\n                <input type=\"text\" placeholder=\"Email\" formControlName=\"email\">\n                <i class=\"fa fa-envelope\"></i>\n            </div>\n\n            <div class=\"error-label\">\n                <label class=\"error\" *ngIf=\"(login.get('password').hasError('required') && (login.get('password').dirty || login.get('password').touched)) || (login.get('password').hasError('required') && !login.get('password').valid && isSubmit)\">Please enter password</label>\n            </div>\n            <div class=\"login-mail\">\n                <input type=\"password\" placeholder=\"Password\" formControlName=\"password\">\n                <i class=\"fa fa-lock\"></i>\n            </div>\n        </div>\n        <div class=\"col-md-6 login-do\">\n            <label class=\"hvr-shutter-in-horizontal login-sub\">\n                <input type=\"submit\" value=\"login\">\n                </label>\n                <p>Do not have an account?</p>\n            <a routerLink=\"/sign-up\" class=\"hvr-shutter-in-horizontal\">Signup</a>\n        </div>\n        \n        <div class=\"clearfix\"> </div>\n        </form>\n    </div>\n</div>\n    <!---->\n<div class=\"copy-right\">\n        <p> &copy; 2019 Apiria. All Rights Reserved </p>\t    </div>  "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/my-claim/my-claim.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/my-claim/my-claim.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"wrapper\">\n        <app-nav></app-nav>\n        <div id=\"page-wrapper\" class=\"gray-bg dashbard-1\">\n            <div class=\"content-main\">\n                <div class=\"banner\">\n                    <h2>\n                        <a href=\"index.html\">Home</a>\n                        <i class=\"fa fa-angle-right\"></i>\n                        <span>My Cliam</span>\n                    </h2>\n                </div>\n                <div class=\"blank\">\n                    <div class=\"blank-page\">\n                        <ag-grid-angular #agGrid style=\"width: 100%; height: 350px;\" class=\"ag-theme-balham\"\n                        [gridOptions]=\"gridOptions\" [pagination]=\"true\" resizable =\"true\" [paginationPageSize]=\"10\" (gridReady)=\"onGridReady($event)\" [rowData]=\"rowData\">\n                        </ag-grid-angular>\n                    </div>\n                    <!-- Button to Open the Modal -->\n                    \n                </div>\n                <div class=\"copy\">\n                    <p> &copy; 2019 Minimal. All Rights Reserved </p>\n                </div>\n            </div>\n        </div>\n        <div class=\"clearfix\"> </div>\n    </div>\n    \n    \n          "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/nav/nav.component.html":
/*!******************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/nav/nav.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar-default navbar-static-top\" role=\"navigation\">\n    <div class=\"navbar-header\">\n        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">\n            <span class=\"sr-only\">Toggle navigation</span>\n            <span class=\"icon-bar\"></span>\n            <span class=\"icon-bar\"></span>\n            <span class=\"icon-bar\"></span>\n        </button>\n        <h1> <a class=\"navbar-brand\" routerLink=\"/pending-claim\">Apiria</a></h1>\n    </div>\n    <div class=\"border-bottom\"></div>\n\n    <!-- Collect the nav links, forms, and other content for toggling -->\n    <div class=\"drop-men\">\n        <ul class=\"nav_1\">\n            <li class=\"dropdown\">\n                <a href=\"#\" class=\"dropdown-toggle dropdown-at\" data-toggle=\"dropdown\"><span class=\"name-caret\">Hi!\n                        {{loggedInUser.data.name}}<i class=\"caret\"></i></span><img></a>\n                <ul class=\"dropdown-menu \" role=\"menu\">\n                    <li  (click)=\"logout()\"><a><i class=\"fa fa-clipboard\"></i>Logout</a></li>\n                </ul>\n            </li>\n        </ul>\n    </div><!-- /.navbar-collapse -->\n    <div class=\"clearfix\"></div>\n\n    <div class=\"navbar-default sidebar\" role=\"navigation\">\n        <div class=\"sidebar-nav navbar-collapse\">\n            <ul class=\"nav\" id=\"side-menu\">\n\n                <li>\n                    <a [attr.href]=\"\" class=\"hvr-bounce-to-right\"><i class=\"fa fa-indent nav_icon\"></i> <span\n                            class=\"nav-label\">Reimbursement</span><span class=\"fa arrow\"></span></a>\n                    <ul class=\"nav nav-second-level\">\n                        <li *ngIf=\"(userRole!=='user')?true:false\">\n                            <a routerLink=\"/pending-claim\" class=\"hvr-bounce-to-right active\">\n                                <i class=\"fa fa-bell nav_icon\"></i>Pending Claims\n                            </a>\n                        </li>\n                        <li *ngIf=\"(userRole!=='user')?true:false\">\n                            <a routerLink=\"/all-claim\" class=\"hvr-bounce-to-right active\">\n                                <i class=\"fa fa-send nav_icon\"></i>All Claims\n                            </a>\n                        </li>\n                        <li>\n                            <a routerLink=\"/my-claims\" class=\"hvr-bounce-to-right active\">\n                                <i class=\"fa fa-hourglass-start nav_icon\"></i>My Claims\n                            </a>\n                        </li>\n                        \n                        <li>\n                            <a routerLink=\"/make-claim\" class=\"hvr-bounce-to-right\">\n                                <i class=\"fa fa-edit nav_icon\"></i>Request Claim\n                            </a>\n                        </li>\n                    </ul>\n                </li>\n                <li *ngIf=\"(userRole=='admin')?true:false\">\n                    <a [attr.href]=\"\" class=\"hvr-bounce-to-right\"><i class=\"fa fa-user nav_icon\"></i> <span\n                            class=\"nav-label\">Manage User</span><span class=\"fa arrow\"></span></a>\n                    <ul class=\"nav nav-second-level\">\n                        <li>\n                            <a routerLink=\"/add-user\" class=\"hvr-bounce-to-right\"> \n                                <i class=\"fa fa-user-plus nav_icon\"></i>Add User\n                            </a>\n                        </li>\n                        <li>\n                            <a routerLink=\"/users-list\" class=\"hvr-bounce-to-right\">\n                                <i class=\"fa fa-users nav_icon\"></i>All User\n                            </a>\n                        </li>\n                    </ul>\n                </li>\n            </ul>\n        </div>\n    </div>\n</nav>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pending-claim/pending-claim.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pending-claim/pending-claim.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"wrapper\">\n        <app-nav></app-nav>\n        <div id=\"page-wrapper\" class=\"gray-bg dashbard-1\">\n            <div class=\"content-main\">\n                <div class=\"banner\">\n                    <h2>\n                        <a href=\"index.html\">Home</a>\n                        <i class=\"fa fa-angle-right\"></i>\n                        <span>Pending Claim</span>\n                    </h2>\n                </div>\n                <div class=\"blank\">\n                    <div class=\"blank-page\">\n                        <ag-grid-angular #agGrid style=\"width: 100%; height: 350px;\" class=\"ag-theme-balham\"\n                        [gridOptions]=\"gridOptions\" resizable =\"true\" [pagination]=\"true\" [paginationPageSize]=\"10\" (gridReady)=\"onGridReady($event)\" [rowData]=\"rowData\">\n                        </ag-grid-angular>\n                    </div>\n                    <!-- Button to Open the Modal -->\n                </div>\n                <div class=\"copy\">\n                    <p> &copy; 2019 Minimal. All Rights Reserved </p>\n                </div>\n            </div>\n        </div>\n        <div class=\"clearfix\"> </div>\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/reimbursement-form/reimbursement-form.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/reimbursement-form/reimbursement-form.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"wrapper\">\n    <app-nav></app-nav>\n    <div id=\"page-wrapper\" class=\"gray-bg dashbard-1\">\n        <div class=\"content-main\">\n            <div class=\"banner\">\n                <h2>\n                    <a href=\"index.html\">Home</a>\n                    <i class=\"fa fa-angle-right\"></i>\n                    <span>Request Reimbursement Claim</span>\n                </h2>\n            </div>\n            <div class=\"blank\">\n                <div class=\"blank-page\">\n                    <div class=\"grid-form1\">\n                        <div class=\"panel-body\">\n                            <form role=\"form\" class=\"form-horizontal\" [formGroup]=\"uploadForm\"\n                                (ngSubmit)=\"uploadSubmit()\">\n                                <!-- <div class=\"form-group has-error\">\n                                <label class=\"col-md-2 control-label\">Input Addon Error</label>\n                                <div class=\"col-md-8\">\n                                    <div class=\"input-group input-icon right\">\n                                        <span class=\"input-group-addon\">\n                                            <i class=\"fa fa-key\"></i>\n                                        </span>\n                                        <input type=\"password\" class=\"form-control1\" placeholder=\"Password\">\n                                    </div>\n                                </div>\n                                <div class=\"col-sm-2\">\n                                    <p class=\"help-block\">Error!</p>\n                                </div>\n                            </div> -->\n\n                                <div class=\"form-group\">\n                                    <label class=\"col-md-3 control-label\">Select Reimbursement Type*</label>\n                                    <div class=\"col-md-7\">\n                                        <div class=\"input-group\">\n                                            <select class=\"form-control1\" formControlName=\"type\">\n                                                <option>Business travel</option>\n                                                <option>Education or training</option>\n                                                <option>Travel</option>\n                                                <option>Medical</option>\n                                                <option>Other</option>\n                                            </select>\n                                        </div>\n                                    </div>\n                                    <div class=\"col-sm-2\">\n                                        <p class=\"help-block\" *ngIf=\"(uploadForm.get('type').hasError('required') && (uploadForm.get('type').dirty || uploadForm.get('type').touched)) || (uploadForm.get('type').hasError('required') && !uploadForm.get('type').valid && isSubmit)\">Please select type</p>\n                                    </div>\n                                </div>\n                                <div class=\"form-group\">\n                                    <label class=\"col-md-3 control-label\">Select Month* </label>\n                                    <div class=\"col-md-7\">\n                                        <div class=\"input-group\">\n                                            <select class=\"form-control1\" formControlName=\"month\">\n                                                <option>January</option>\n                                                <option>February</option>\n                                                <option>March</option>\n                                                <option>April</option>\n                                                <option>May</option>\n                                                <option>June</option>\n                                                <option>July</option>\n                                                <option>August</option>\n                                                <option>September</option>\n                                                <option>October</option>\n                                                <option>November</option>\n                                                <option>December</option>\n                                            </select>\n                                        </div>\n                                    </div>\n                                    <div class=\"col-sm-2\">\n                                        <p class=\"help-block\" *ngIf=\"(uploadForm.get('month').hasError('required') && (uploadForm.get('month').dirty || uploadForm.get('month').touched)) || (uploadForm.get('month').hasError('required') && !uploadForm.get('month').valid && isSubmit)\">Please select month</p>\n                                    </div>\n                                </div>\n\n                                \n\n                                <div class=\"form-group\">\n                                    <label class=\"col-md-3 control-label\"> ₹ Amount*</label>\n                                    <div class=\"col-md-7\">\n                                        <div class=\"input-group\">\n                                            <input type=\"number\" formControlName=\"amount\" class=\"form-control1\">\n                                        </div>\n                                    </div>\n                                    <div class=\"col-sm-2\">\n                                        <p class=\"help-block\" *ngIf=\"(uploadForm.get('amount').hasError('required') && (uploadForm.get('amount').dirty || uploadForm.get('amount').touched)) || (uploadForm.get('amount').hasError('required') && !uploadForm.get('amount').valid && isSubmit)\">Please enter amount</p>\n                                    </div>\n                                </div>\n\n                                <div class=\"form-group\">\n                                    <label class=\"col-md-3 control-label\">Add file*</label>\n                                    <div class=\"col-md-7\">\n                                        <div class=\"input-group\">\n                                            <!-- <input type=\"file\" id=\"exampleInputFile\"> -->\n                                            <input type=\"text\" class=\"form-control1\" value=\"{{filesName}}\" disabled>\n                                            <div class=\"input-group-addon file-attach\">\n                                                <input type=\"file\" (change)=\"fileChange($event)\" formControlName=\"document\" type=\"file\" \n                                                multiple id=\"document\"  accept=\"application/msword, application/vnd.ms-excel, \n                                                text/plain, application/pdf, image/*\"> <i class=\"fa fa-file\"></i> Select File </div>\n                                            <!-- <div class=\"input-group-addon addFileButton\"><i class=\"fa fa-plus\"></i>\n                                            </div> -->\n                                        </div>\n                                    </div>\n                                    <div class=\"col-sm-2\">\n                                        <p class=\"help-block\" *ngIf=\"(uploadForm.get('document').hasError('required') && (uploadForm.get('document').dirty || uploadForm.get('document').touched)) || (uploadForm.get('document').hasError('required') && !uploadForm.get('document').valid && isSubmit)\">Please attach document</p>\n                                    </div>\n                                </div>\n                                <!-- <div class=\"form-group\">\n                                        <div class=\"col-md-7\">\n                                    <table>\n                                        <thead>\n                                            <tr>\n                                                <th width=\"90%\">\n                                                    File Name\n                                                </th>\n                                                <th width=\"10%\">\n                                                    Remove\n                                                </th>\n                                            </tr>\n                                        </thead>\n                                        <tbody>\n                                            <tr *ngFor=\"let item of uploader.queue\">\n                                                <th width=\"90%\">\n                                                    {{item._file.name}} {{item._file.size}}\n                                                </th>\n                                                <th class=\"text-center\" width=\"10%\">\n                                                    <i class=\"fa fa-trash\" (click)=\"item.remove()\">delete</i>\n                                                </th>\n                                            </tr>\n                                        </tbody>\n                                    </table>\n                                    </div>\n                                </div> -->\n\n                                <div class=\"form-group\">\n                                    <label class=\"col-md-3 control-label\">Comment*</label>\n                                    <div class=\"col-md-7\">\n                                        <div class=\"input-group\">\n                                            <textarea formControlName=\"comment\" class=\"form-control1\"></textarea>\n                                        </div>\n                                    </div>\n                                    <div class=\"col-sm-2\">\n                                        <p class=\"help-block\" *ngIf=\"(uploadForm.get('comment').hasError('required') && (uploadForm.get('comment').dirty || uploadForm.get('comment').touched)) || (uploadForm.get('comment').hasError('required') && !uploadForm.get('comment').valid && isSubmit)\">Please enter comment</p>\n                                    </div>\n                                </div>\n\n\n                                <!-- <div class=\"form-group\">\n                                <label class=\"col-md-2 control-label\">Input Processing</label>\n                                <div class=\"col-md-8\">\n                                    <div class=\"input-icon right spinner\">\n                                        <i class=\"fa fa-fw fa-spin fa-spinner\"></i>\n                                        <input id=\"email\" class=\"form-control1\" type=\"text\" placeholder=\"Processing...\">\n                                    </div>\n                                </div>\n                                <div class=\"col-sm-2\">\n                                    <p class=\"help-block\">Processing right</p>\n                                </div>\n                            </div> -->\n\n\n                                <div class=\"panel-footer\">\n                                    <div class=\"row\">\n                                        <div class=\"col-sm-8 col-sm-offset-2\">\n                                            <button type=\"submit\" class=\"btn-primary btn\">Submit</button>\n                                            <button class=\"btn-default btn\">Cancel</button>\n                                            <button class=\"btn-inverse btn\">Reset</button>\n                                        </div>\n                                    </div>\n                                </div>\n                            </form>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"copy\">\n                <p> &copy; 2019 Minimal. All Rights Reserved </p>\n            </div>\n        </div>\n    </div>\n    <div class=\"clearfix\"> </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/signup/signup.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/signup/signup.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login\">\n    <h1><a [attr.href]=\"\">Apiria</a></h1>\n    <div class=\"login-bottom\">\n        <h2>Signup</h2>\n        <form [formGroup]=\"signup\" (ngSubmit)=\"signupSubmit()\">\n        <div class=\"col-md-6\">\n            <div class=\"error-label\">\n            <label class=\"error\" *ngIf=\"(signup.get('name').hasError('required') && (signup.get('name').dirty || signup.get('name').touched)) || (signup.get('name').hasError('required') && !signup.get('name').valid && isSubmit)\">Please enter name</label>\n            </div>\n            <div class=\"login-mail\">\n                <input type=\"text\" placeholder=\"Name\" formControlName=\"name\">\n                <i class=\"fa fa-user\"></i>\n            </div>\n\n            <div class=\"error-label\">\n            <label class=\"error\" *ngIf=\"(signup.get('email').hasError('required') && (signup.get('email').dirty || signup.get('email').touched)) || (signup.get('email').hasError('required') && !signup.get('email').valid && isSubmit)\">Please enter email</label>\n            </div>\n            <div class=\"login-mail\">\n                <input type=\"text\" placeholder=\"Email\" formControlName=\"email\">\n                <i class=\"fa fa-envelope\"></i>\n            </div>\n\n            <div class=\"error-label\">\n            <label class=\"error\" *ngIf=\"(signup.get('password').hasError('required') && (signup.get('password').dirty || signup.get('password').touched)) || (signup.get('password').hasError('required') && !signup.get('password').valid && isSubmit)\">Please enter password</label>\n            </div>\n            <div class=\"login-mail\">\n                <input type=\"password\" placeholder=\"Password\" formControlName=\"password\">\n                <i class=\"fa fa-lock\"></i>\n            </div>\n\n            <div class=\"error-label\">\n            <label class=\"error\" *ngIf=\"(signup.get('repassword').hasError('required') && (signup.get('repassword').dirty || signup.get('repassword').touched)) || (signup.get('repassword').hasError('required') && !signup.get('repassword').valid && isSubmit)\">Please re-enter password</label>\n            <label class=\"error\" *ngIf=\"confirmPassErr\">Passwords do not match</label>\n            </div>\n            <div class=\"login-mail\">\n                <input type=\"password\" placeholder=\"Repeated password\" formControlName=\"repassword\">\n                <i class=\"fa fa-lock\"></i>\n            </div>\n\n            <!-- <label class=\"error\" *ngIf=\"signup.get('role').hasError('required') && (signup.get('role').dirty || signup.get('role').touched)\">Do not have an account?</label>\n            <div class=\"login-mail\">\n                <select multiple name=\"selector1\" id=\"selector1\" class=\"form-control1\" formControlName=\"role\">\n                    <option disabled> Select user type </option>  \n                    <option> Admin </option>\n                    <option> Manager </option>\n                    <option> User </option>\n                </select>\n                <i class=\"fa fa-user\"></i>\n            </div> -->\n\n            <div class=\"error-label\">\n            <label class=\"error\" *ngIf=\"signup.get('term').hasError('required') && (signup.get('term').dirty || signup.get('term').touched)\">Do not have an account?</label>\n            </div>\n            <a class=\"news-letter\" href=\"#\">\n                <label class=\"checkbox1\"><input type=\"checkbox\" name=\"checkbox\" formControlName=\"term\"><i> </i>I agree with the terms</label>\n            </a>\n\n        </div>\n        <div class=\"col-md-6 login-do\">\n            <label class=\"hvr-shutter-in-horizontal login-sub\">\n                <input type=\"submit\" value=\"Signup\">\n                </label>\n                <p>Do not have an account?</p>\n            <a routerLink=\"/login\" class=\"hvr-shutter-in-horizontal\">Login</a>\n        </div>\n        \n        <div class=\"clearfix\"> </div>\n        </form>\n    </div>\n</div>\n    <!---->\n<div class=\"copy-right\">\n        <p> &copy; 2019 Apiria. All Rights Reserved </p>\t    </div>  "

/***/ }),

/***/ "./src/app/add-user/add-user.component.css":
/*!*************************************************!*\
  !*** ./src/app/add-user/add-user.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".help-block{\r\n    color: #e00707\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRkLXVzZXIvYWRkLXVzZXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9hZGQtdXNlci9hZGQtdXNlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlbHAtYmxvY2t7XHJcbiAgICBjb2xvcjogI2UwMDcwN1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/add-user/add-user.component.ts":
/*!************************************************!*\
  !*** ./src/app/add-user/add-user.component.ts ***!
  \************************************************/
/*! exports provided: AddUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddUserComponent", function() { return AddUserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_client_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/client.service */ "./src/app/services/client.service.ts");




var AddUserComponent = /** @class */ (function () {
    function AddUserComponent(fb, clientService) {
        this.fb = fb;
        this.clientService = clientService;
        this.isSubmit = false;
        this.managers = [];
    }
    AddUserComponent.prototype.ngOnInit = function () {
        this.addUser = this.fb.group({
            name: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            email: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            password: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            manager: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            role: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])]
        });
        this.allManager();
        this.changeValidation("");
    };
    AddUserComponent.prototype.allManager = function () {
        var _this = this;
        this.clientService.allManager().subscribe(function (response) {
            //console.log(response);
            if (response.status) {
                _this.managers = response.data;
            }
        });
    };
    AddUserComponent.prototype.registerSubmit = function () {
        var _this = this;
        this.isSubmit = true;
        var manager;
        if (this.addUser.valid) {
            var datetime = new Date();
            if (this.addUser.controls.role.value === "user") {
                manager = this.addUser.controls.manager.value;
            }
            else {
                manager = null;
            }
            var formData = {
                name: this.addUser.controls.name.value,
                email: this.addUser.controls.email.value,
                password: this.addUser.controls.password.value,
                role: this.addUser.controls.role.value,
                manager: manager,
                createddate: datetime,
                updateddate: datetime
            };
            this.clientService.addUser(formData).subscribe(function (response) {
                //console.log(response);
                _this.addUser.reset();
                _this.isSubmit = false;
                if (response.status) {
                    alert('User added..');
                }
            });
        }
    };
    AddUserComponent.prototype.changeValidation = function (role) {
        var managerControle = this.addUser.get('manager');
        if (role !== 'user') {
            managerControle.clearValidators();
            managerControle.disable();
        }
        else {
            managerControle.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
            managerControle.enable();
        }
        managerControle.updateValueAndValidity();
    };
    AddUserComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _services_client_service__WEBPACK_IMPORTED_MODULE_3__["ClientService"] }
    ]; };
    AddUserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add-user',
            template: __webpack_require__(/*! raw-loader!./add-user.component.html */ "./node_modules/raw-loader/index.js!./src/app/add-user/add-user.component.html"),
            styles: [__webpack_require__(/*! ./add-user.component.css */ "./src/app/add-user/add-user.component.css")]
        })
    ], AddUserComponent);
    return AddUserComponent;
}());



/***/ }),

/***/ "./src/app/ag-action/click-button.parent/click-button.parent.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/ag-action/click-button.parent/click-button.parent.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FnLWFjdGlvbi9jbGljay1idXR0b24ucGFyZW50L2NsaWNrLWJ1dHRvbi5wYXJlbnQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/ag-action/click-button.parent/click-button.parent.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/ag-action/click-button.parent/click-button.parent.component.ts ***!
  \********************************************************************************/
/*! exports provided: ClickButtonParentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClickButtonParentComponent", function() { return ClickButtonParentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_claim_request_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/claim-request.service */ "./src/app/services/claim-request.service.ts");



var ClickButtonParentComponent = /** @class */ (function () {
    function ClickButtonParentComponent(claimRequestService) {
        this.claimRequestService = claimRequestService;
        //@Input() responseData: any;
        this.responseData = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ClickButtonParentComponent.prototype.agInit = function (params) {
        this.params = params;
        this.cell = { row: params.value, col: params.colDef.headerName, other: params.row };
    };
    ClickButtonParentComponent.prototype.clicked = function (cell) {
        var _this = this;
        //console.log(this.params.data )
        //console.log("Child Cell Clicked: " + JSON.stringify(cell));
        var data = { _id: this.params.data._id, status: cell.actions, updateddate: new Date() };
        this.claimRequestService.claimAction(data).subscribe(function (res) {
            var response = res;
            //console.log(response);
            _this.responseData.emit(response);
            //this.rowData= response.data;
        });
    };
    ClickButtonParentComponent.prototype.refresh = function () {
        return false;
    };
    ClickButtonParentComponent.ctorParameters = function () { return [
        { type: _services_claim_request_service__WEBPACK_IMPORTED_MODULE_2__["ClaimRequestService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], ClickButtonParentComponent.prototype, "responseData", void 0);
    ClickButtonParentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-click-button.parent',
            template: __webpack_require__(/*! raw-loader!./click-button.parent.component.html */ "./node_modules/raw-loader/index.js!./src/app/ag-action/click-button.parent/click-button.parent.component.html"),
            styles: [__webpack_require__(/*! ./click-button.parent.component.css */ "./src/app/ag-action/click-button.parent/click-button.parent.component.css")]
        })
    ], ClickButtonParentComponent);
    return ClickButtonParentComponent;
}());



/***/ }),

/***/ "./src/app/ag-action/click-button/click-button.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/ag-action/click-button/click-button.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn {\r\n    line-height: 0.5;\r\n    width: 50%;\r\n    background: transparent;\r\n}\r\n\r\n.green{\r\n    color:green;\r\n}\r\n\r\n.red{\r\n    color:red;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWctYWN0aW9uL2NsaWNrLWJ1dHRvbi9jbGljay1idXR0b24uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGdCQUFnQjtJQUNoQixVQUFVO0lBQ1YsdUJBQXVCO0FBQzNCOztBQUVBO0lBQ0ksV0FBVztBQUNmOztBQUVBO0lBQ0ksU0FBUztBQUNiIiwiZmlsZSI6InNyYy9hcHAvYWctYWN0aW9uL2NsaWNrLWJ1dHRvbi9jbGljay1idXR0b24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idG4ge1xyXG4gICAgbGluZS1oZWlnaHQ6IDAuNTtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxufVxyXG5cclxuLmdyZWVue1xyXG4gICAgY29sb3I6Z3JlZW47XHJcbn1cclxuXHJcbi5yZWR7XHJcbiAgICBjb2xvcjpyZWQ7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/ag-action/click-button/click-button.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/ag-action/click-button/click-button.component.ts ***!
  \******************************************************************/
/*! exports provided: ClickButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClickButtonComponent", function() { return ClickButtonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ClickButtonComponent = /** @class */ (function () {
    function ClickButtonComponent() {
        this.onClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ClickButtonComponent.prototype.click = function (action) {
        this.cell.actions = action;
        this.onClicked.emit(this.cell);
    };
    ClickButtonComponent.prototype.ngOnInit = function () {
    };
    ClickButtonComponent.prototype.ngOnChange = function () {
        // console.log({"this.responseData":this.responseData});
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], ClickButtonComponent.prototype, "cell", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
    ], ClickButtonComponent.prototype, "responseData", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
    ], ClickButtonComponent.prototype, "onClicked", void 0);
    ClickButtonComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-click-button',
            template: __webpack_require__(/*! raw-loader!./click-button.component.html */ "./node_modules/raw-loader/index.js!./src/app/ag-action/click-button/click-button.component.html"),
            styles: [__webpack_require__(/*! ./click-button.component.css */ "./src/app/ag-action/click-button/click-button.component.css")]
        })
    ], ClickButtonComponent);
    return ClickButtonComponent;
}());



/***/ }),

/***/ "./src/app/ag-action/date-pipe/date-pipe.component.css":
/*!*************************************************************!*\
  !*** ./src/app/ag-action/date-pipe/date-pipe.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FnLWFjdGlvbi9kYXRlLXBpcGUvZGF0ZS1waXBlLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/ag-action/date-pipe/date-pipe.component.ts":
/*!************************************************************!*\
  !*** ./src/app/ag-action/date-pipe/date-pipe.component.ts ***!
  \************************************************************/
/*! exports provided: DatePipeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatePipeComponent", function() { return DatePipeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DatePipeComponent = /** @class */ (function () {
    function DatePipeComponent() {
    }
    DatePipeComponent.prototype.agInit = function (params) {
        this.params = params;
    };
    DatePipeComponent.prototype.refresh = function () {
        return false;
    };
    DatePipeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-date-pipe',
            template: __webpack_require__(/*! raw-loader!./date-pipe.component.html */ "./node_modules/raw-loader/index.js!./src/app/ag-action/date-pipe/date-pipe.component.html"),
            styles: [__webpack_require__(/*! ./date-pipe.component.css */ "./src/app/ag-action/date-pipe/date-pipe.component.css")]
        })
    ], DatePipeComponent);
    return DatePipeComponent;
}());



/***/ }),

/***/ "./src/app/all-user/all-user.component.css":
/*!*************************************************!*\
  !*** ./src/app/all-user/all-user.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FsbC11c2VyL2FsbC11c2VyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/all-user/all-user.component.ts":
/*!************************************************!*\
  !*** ./src/app/all-user/all-user.component.ts ***!
  \************************************************/
/*! exports provided: AllUserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllUserComponent", function() { return AllUserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_client_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/client.service */ "./src/app/services/client.service.ts");



var AllUserComponent = /** @class */ (function () {
    function AllUserComponent(clientService) {
        this.clientService = clientService;
        this.rowData = [];
        this.gridOptions = {};
        this.gridOptions.columnDefs = [
            {
                headerName: "Name",
                field: "name",
                width: 150,
                sortable: true,
                filter: true
            },
            {
                headerName: "Email ID",
                field: "email",
                width: 150,
                sortable: true,
                filter: true
            },
            {
                headerName: "Role",
                field: "role",
                width: 150,
                sortable: true,
                filter: true
            },
            {
                headerName: "Manager",
                field: "manager",
                width: 150,
                sortable: true,
                filter: true
            }
        ];
        // this.gridOptions.rowData = [
        //     // {name: "5" , email: "test1", manager:null},
        //     // {name: "10", email: "test1", manager:"Anull"},
        //     // {name: "15", email: "test1", manager:null}
        // ];
    }
    AllUserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.clientService.getUsers().subscribe(function (res) {
            var response = res;
            //console.log(response);
            _this.rowData = response.data;
        });
    };
    AllUserComponent.prototype.onGridReady = function (params) {
    };
    AllUserComponent.ctorParameters = function () { return [
        { type: _services_client_service__WEBPACK_IMPORTED_MODULE_2__["ClientService"] }
    ]; };
    AllUserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-all-user',
            template: __webpack_require__(/*! raw-loader!./all-user.component.html */ "./node_modules/raw-loader/index.js!./src/app/all-user/all-user.component.html"),
            styles: [__webpack_require__(/*! ./all-user.component.css */ "./src/app/all-user/all-user.component.css")]
        })
    ], AllUserComponent);
    return AllUserComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _guard_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./guard/auth.guard */ "./src/app/guard/auth.guard.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _reimbursement_form_reimbursement_form_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./reimbursement-form/reimbursement-form.component */ "./src/app/reimbursement-form/reimbursement-form.component.ts");
/* harmony import */ var _all_user_all_user_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./all-user/all-user.component */ "./src/app/all-user/all-user.component.ts");
/* harmony import */ var _add_user_add_user_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./add-user/add-user.component */ "./src/app/add-user/add-user.component.ts");
/* harmony import */ var _pending_claim_pending_claim_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pending-claim/pending-claim.component */ "./src/app/pending-claim/pending-claim.component.ts");
/* harmony import */ var _my_claim_my_claim_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./my-claim/my-claim.component */ "./src/app/my-claim/my-claim.component.ts");












var routes = [
    {
        path: '',
        redirectTo: 'login', pathMatch: 'full'
    },
    {
        path: 'login',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"]
    },
    {
        path: 'sign-up',
        component: _signup_signup_component__WEBPACK_IMPORTED_MODULE_5__["SignupComponent"]
    },
    {
        path: 'all-claim',
        component: _home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"],
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]]
    },
    {
        path: 'pending-claim',
        component: _pending_claim_pending_claim_component__WEBPACK_IMPORTED_MODULE_10__["PendingClaimComponent"],
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]]
    },
    {
        path: 'my-claims',
        component: _my_claim_my_claim_component__WEBPACK_IMPORTED_MODULE_11__["MyClaimComponent"],
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]]
    },
    {
        path: 'make-claim',
        component: _reimbursement_form_reimbursement_form_component__WEBPACK_IMPORTED_MODULE_7__["ReimbursementFormComponent"],
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]]
    },
    {
        path: 'users-list',
        component: _all_user_all_user_component__WEBPACK_IMPORTED_MODULE_8__["AllUserComponent"],
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]]
    },
    {
        path: 'add-user',
        component: _add_user_add_user_component__WEBPACK_IMPORTED_MODULE_9__["AddUserComponent"],
        canActivate: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'ApiriaClient';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ag_grid_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ag-grid-angular */ "./node_modules/ag-grid-angular/main.js");
/* harmony import */ var ag_grid_angular__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ag_grid_angular__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _guard_auth_guard__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./guard/auth.guard */ "./src/app/guard/auth.guard.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _ag_action_click_button_click_button_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./ag-action/click-button/click-button.component */ "./src/app/ag-action/click-button/click-button.component.ts");
/* harmony import */ var _ag_action_click_button_parent_click_button_parent_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./ag-action/click-button.parent/click-button.parent.component */ "./src/app/ag-action/click-button.parent/click-button.parent.component.ts");
/* harmony import */ var _nav_nav_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./nav/nav.component */ "./src/app/nav/nav.component.ts");
/* harmony import */ var _reimbursement_form_reimbursement_form_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./reimbursement-form/reimbursement-form.component */ "./src/app/reimbursement-form/reimbursement-form.component.ts");
/* harmony import */ var _add_user_add_user_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./add-user/add-user.component */ "./src/app/add-user/add-user.component.ts");
/* harmony import */ var _all_user_all_user_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./all-user/all-user.component */ "./src/app/all-user/all-user.component.ts");
/* harmony import */ var _pending_claim_pending_claim_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./pending-claim/pending-claim.component */ "./src/app/pending-claim/pending-claim.component.ts");
/* harmony import */ var _my_claim_my_claim_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./my-claim/my-claim.component */ "./src/app/my-claim/my-claim.component.ts");
/* harmony import */ var _ag_action_date_pipe_date_pipe_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./ag-action/date-pipe/date-pipe.component */ "./src/app/ag-action/date-pipe/date-pipe.component.ts");






















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_10__["LoginComponent"],
                _signup_signup_component__WEBPACK_IMPORTED_MODULE_11__["SignupComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_12__["HomeComponent"],
                _ag_action_click_button_click_button_component__WEBPACK_IMPORTED_MODULE_13__["ClickButtonComponent"],
                _ag_action_click_button_parent_click_button_parent_component__WEBPACK_IMPORTED_MODULE_14__["ClickButtonParentComponent"],
                _nav_nav_component__WEBPACK_IMPORTED_MODULE_15__["NavComponent"],
                _reimbursement_form_reimbursement_form_component__WEBPACK_IMPORTED_MODULE_16__["ReimbursementFormComponent"],
                ng2_file_upload__WEBPACK_IMPORTED_MODULE_6__["FileSelectDirective"],
                _add_user_add_user_component__WEBPACK_IMPORTED_MODULE_17__["AddUserComponent"],
                _all_user_all_user_component__WEBPACK_IMPORTED_MODULE_18__["AllUserComponent"],
                _pending_claim_pending_claim_component__WEBPACK_IMPORTED_MODULE_19__["PendingClaimComponent"],
                _my_claim_my_claim_component__WEBPACK_IMPORTED_MODULE_20__["MyClaimComponent"],
                _ag_action_date_pipe_date_pipe_component__WEBPACK_IMPORTED_MODULE_21__["DatePipeComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_7__["AppRoutingModule"],
                ag_grid_angular__WEBPACK_IMPORTED_MODULE_3__["AgGridModule"].withComponents([_ag_action_click_button_parent_click_button_parent_component__WEBPACK_IMPORTED_MODULE_14__["ClickButtonParentComponent"], _ag_action_date_pipe_date_pipe_component__WEBPACK_IMPORTED_MODULE_21__["DatePipeComponent"]]),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"]
            ],
            providers: [_guard_auth_guard__WEBPACK_IMPORTED_MODULE_8__["AuthGuard"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/guard/auth.guard.ts":
/*!*************************************!*\
  !*** ./src/app/guard/auth.guard.ts ***!
  \*************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        //console.log({"route":route});
        //console.log({"state":state.url});
        var user = JSON.parse(sessionStorage.getItem('loggedInUser'));
        if (user) {
            if ((state.url === '/all-claim' || state.url === '/pending-claim' || state.url === '/all-claim' || state.url === '/add-user' || state.url === '/users-list') && user.data.role === 'user') {
                return this.router.parseUrl('/my-claims');
            }
            else if ((state.url === '/add-user' || state.url === '/users-list') && user.data.role === 'manager') {
                return this.router.parseUrl('/pending-claim');
            }
            else {
                return true;
            }
        }
        else {
            return this.router.parseUrl('/login');
        }
    };
    AuthGuard.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        })
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn {\r\n    line-height: 0.5;\r\n    width: 50%;\r\n    background: transparent;\r\n}\r\n\r\n.green{\r\n    color:green;\r\n}\r\n\r\n.red{\r\n    color:red;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxnQkFBZ0I7SUFDaEIsVUFBVTtJQUNWLHVCQUF1QjtBQUMzQjs7QUFFQTtJQUNJLFdBQVc7QUFDZjs7QUFFQTtJQUNJLFNBQVM7QUFDYiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ0biB7XHJcbiAgICBsaW5lLWhlaWdodDogMC41O1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG59XHJcblxyXG4uZ3JlZW57XHJcbiAgICBjb2xvcjpncmVlbjtcclxufVxyXG5cclxuLnJlZHtcclxuICAgIGNvbG9yOnJlZDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_claim_request_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/claim-request.service */ "./src/app/services/claim-request.service.ts");



var HomeComponent = /** @class */ (function () {
    //https://www.ag-grid.com/angular-getting-started/
    function HomeComponent(claimRequestService) {
        this.claimRequestService = claimRequestService;
        this.rowData = [];
        this.gridOptions = {};
        this.gridOptions.columnDefs = [
            {
                headerName: "User",
                field: "user",
                width: 180,
                sortable: true,
                filter: true
            },
            {
                headerName: "Reimbursement Type",
                field: "type",
                width: 150,
                sortable: true,
                filter: true
            },
            {
                headerName: "Month",
                field: "month",
                width: 100,
                sortable: true,
                filter: true
            },
            {
                headerName: "Amount",
                field: "amount",
                width: 100,
                sortable: true,
                filter: true
            },
            {
                headerName: "Status",
                field: "status",
                width: 100,
                sortable: true,
                filter: true
            },
            {
                headerName: "Created Date",
                field: "createddate",
                width: 180,
                sortable: true,
                filter: true
            },
            {
                headerName: "Updated Date",
                field: "updateddate",
                width: 180,
                sortable: true,
                filter: true
            },
        ];
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.getClaim();
    };
    HomeComponent.prototype.onGridReady = function (params) {
    };
    HomeComponent.prototype.getClaim = function () {
        var _this = this;
        this.claimRequestService.getClaim().subscribe(function (res) {
            var response = res;
            //console.log(response);
            _this.rowData = response.data;
        });
    };
    HomeComponent.prototype.clickCell = function (action, data) {
        var _this = this;
        if (data && (data.status == 'approve' || data.status == 'reject')) {
            data = { _id: data._id, status: action, updateddate: new Date() };
            //console.log(data)
            this.claimRequestService.claimAction(data).subscribe(function (res) {
                var response = res;
                //console.log(response);
                _this.getClaim();
            });
        }
    };
    HomeComponent.ctorParameters = function () { return [
        { type: _services_claim_request_service__WEBPACK_IMPORTED_MODULE_2__["ClaimRequestService"] }
    ]; };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
        })
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".error {\r\n    font-size: 13px;\r\n    color: #e00707;\r\n    font-weight: 100;\r\n}\r\n\r\n.error-label{\r\n    height: 25px;\r\n}\r\n\r\n.login-mail {\r\n    margin-bottom: 0.6em;\r\n}\r\n\r\n.login-do{\r\n    padding: 22px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGVBQWU7SUFDZixjQUFjO0lBQ2QsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksWUFBWTtBQUNoQjs7QUFFQTtJQUNJLG9CQUFvQjtBQUN4Qjs7QUFFQTtJQUNJLGFBQWE7QUFDakIiLCJmaWxlIjoic3JjL2FwcC9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVycm9yIHtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIGNvbG9yOiAjZTAwNzA3O1xyXG4gICAgZm9udC13ZWlnaHQ6IDEwMDtcclxufVxyXG5cclxuLmVycm9yLWxhYmVse1xyXG4gICAgaGVpZ2h0OiAyNXB4O1xyXG59XHJcblxyXG4ubG9naW4tbWFpbCB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwLjZlbTtcclxufVxyXG5cclxuLmxvZ2luLWRve1xyXG4gICAgcGFkZGluZzogMjJweDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/authentication.service */ "./src/app/services/authentication.service.ts");




var LoginComponent = /** @class */ (function () {
    function LoginComponent(fb, auth) {
        this.fb = fb;
        this.auth = auth;
        this.isSubmit = false;
        // let data = {"email":"bob@gmail.com","password":"111111"}
        // this.auth.login(data);
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.login = this.fb.group({
            password: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            email: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])]
        });
    };
    LoginComponent.prototype.loginSubmit = function () {
        //console.log(this.login);
        this.isSubmit = true;
        if (this.login.valid) {
            var data = { "email": this.login.controls.email.value, "password": this.login.controls.password.value };
            this.auth.login(data);
        }
    };
    LoginComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] }
    ]; };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        })
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/my-claim/my-claim.component.css":
/*!*************************************************!*\
  !*** ./src/app/my-claim/my-claim.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL215LWNsYWltL215LWNsYWltLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/my-claim/my-claim.component.ts":
/*!************************************************!*\
  !*** ./src/app/my-claim/my-claim.component.ts ***!
  \************************************************/
/*! exports provided: MyClaimComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyClaimComponent", function() { return MyClaimComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_claim_request_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/claim-request.service */ "./src/app/services/claim-request.service.ts");



var MyClaimComponent = /** @class */ (function () {
    //https://www.ag-grid.com/angular-getting-started/
    function MyClaimComponent(claimRequestService) {
        var _this = this;
        this.claimRequestService = claimRequestService;
        this.rowData = [];
        this.gridOptions = {};
        this.gridOptions.columnDefs = [
            {
                headerName: "Reimbursement Type",
                field: "type",
                width: 150,
                sortable: true,
                filter: true
            },
            {
                headerName: "Month",
                field: "month",
                width: 100,
                sortable: true,
                filter: true
            },
            {
                headerName: "Amount",
                field: "amount",
                width: 100,
                sortable: true,
                filter: true
            },
            {
                headerName: "Status",
                field: "status",
                width: 100,
                sortable: true,
                filter: true
            },
            {
                headerName: "Created Date",
                field: "createddate",
                width: 180,
                sortable: true,
                filter: true
            },
            {
                headerName: "Updated Date",
                field: "updateddate",
                width: 180,
                sortable: true,
                filter: true
            },
            {
                headerName: "",
                field: "action",
                width: 35,
                cellClass: ['ag-cell-custom'],
                onCellClicked: function (params) {
                    _this.clickCell("remove", params.data);
                },
                cellRenderer: function (params) {
                    //console.log(params.data._id);
                    if (params.data.status == 'pending') {
                        return '<button class="btn btn-action" title="Remove"><i class="fa fa-trash green"></i></button>';
                    }
                }
            }
        ];
    }
    MyClaimComponent.prototype.ngOnInit = function () {
        this.getMyClaim();
    };
    MyClaimComponent.prototype.onGridReady = function (params) {
    };
    MyClaimComponent.prototype.getMyClaim = function () {
        var _this = this;
        this.claimRequestService.getMyClaim().subscribe(function (res) {
            var response = res;
            //console.log(response);
            _this.rowData = response.data;
        });
    };
    MyClaimComponent.prototype.clickCell = function (action, data) {
        var _this = this;
        if (data && (data.status == 'pending')) {
            //console.log(data);
            data = { _id: data._id, status: action, updateddate: new Date() };
            //console.log(data)
            this.claimRequestService.claimAction(data).subscribe(function (res) {
                var response = res;
                //console.log(response);
                _this.getMyClaim();
            });
        }
    };
    MyClaimComponent.ctorParameters = function () { return [
        { type: _services_claim_request_service__WEBPACK_IMPORTED_MODULE_2__["ClaimRequestService"] }
    ]; };
    MyClaimComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-my-claim',
            template: __webpack_require__(/*! raw-loader!./my-claim.component.html */ "./node_modules/raw-loader/index.js!./src/app/my-claim/my-claim.component.html"),
            styles: [__webpack_require__(/*! ./my-claim.component.css */ "./src/app/my-claim/my-claim.component.css")]
        })
    ], MyClaimComponent);
    return MyClaimComponent;
}());



/***/ }),

/***/ "./src/app/nav/nav.component.css":
/*!***************************************!*\
  !*** ./src/app/nav/nav.component.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25hdi9uYXYuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/nav/nav.component.ts":
/*!**************************************!*\
  !*** ./src/app/nav/nav.component.ts ***!
  \**************************************/
/*! exports provided: NavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavComponent", function() { return NavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var NavComponent = /** @class */ (function () {
    function NavComponent(router) {
        this.router = router;
        this.loggedInUser = JSON.parse(sessionStorage.getItem("loggedInUser"));
        //console.log(this.loggedInUser);
        this.userRole = this.loggedInUser.data.role;
        //console.log({"role":this.userRole})
    }
    NavComponent.prototype.ngOnInit = function () {
    };
    NavComponent.prototype.logout = function () {
        sessionStorage.clear();
        this.router.navigate(['../'], {});
    };
    NavComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    NavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-nav',
            template: __webpack_require__(/*! raw-loader!./nav.component.html */ "./node_modules/raw-loader/index.js!./src/app/nav/nav.component.html"),
            styles: [__webpack_require__(/*! ./nav.component.css */ "./src/app/nav/nav.component.css")]
        })
    ], NavComponent);
    return NavComponent;
}());



/***/ }),

/***/ "./src/app/pending-claim/pending-claim.component.css":
/*!***********************************************************!*\
  !*** ./src/app/pending-claim/pending-claim.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BlbmRpbmctY2xhaW0vcGVuZGluZy1jbGFpbS5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/pending-claim/pending-claim.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/pending-claim/pending-claim.component.ts ***!
  \**********************************************************/
/*! exports provided: PendingClaimComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PendingClaimComponent", function() { return PendingClaimComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_claim_request_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/claim-request.service */ "./src/app/services/claim-request.service.ts");
/* harmony import */ var _ag_action_date_pipe_date_pipe_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ag-action/date-pipe/date-pipe.component */ "./src/app/ag-action/date-pipe/date-pipe.component.ts");




var PendingClaimComponent = /** @class */ (function () {
    //https://www.ag-grid.com/angular-getting-started/
    function PendingClaimComponent(claimRequestService) {
        var _this = this;
        this.claimRequestService = claimRequestService;
        this.rowData = [];
        this.gridOptions = {};
        this.gridOptions.columnDefs = [
            {
                headerName: "User",
                field: "user",
                width: 150,
                sortable: true,
                filter: true
            },
            {
                headerName: "Type",
                field: "type",
                width: 100,
                sortable: true,
                filter: true
            },
            {
                headerName: "Month",
                field: "month",
                width: 100,
                sortable: true,
                filter: true
            },
            {
                headerName: "Amount",
                field: "amount",
                width: 100,
                sortable: true,
                filter: true
            },
            {
                headerName: "Status",
                field: "status",
                width: 100,
                sortable: true,
                filter: true
            },
            {
                headerName: "Created Date",
                field: "createddate",
                width: 180,
                sortable: true,
                filter: true,
                cellEditorFramework: _ag_action_date_pipe_date_pipe_component__WEBPACK_IMPORTED_MODULE_3__["DatePipeComponent"]
            },
            {
                headerName: "Updated Date",
                field: "updateddate",
                width: 180,
                sortable: true,
                filter: true
            },
            {
                headerName: "",
                field: "action",
                width: 35,
                resizable: false,
                cellClass: ['ag-cell-custom'],
                onCellClicked: function (params) {
                    _this.clickCell("approve", params.data);
                },
                cellRenderer: function (params) {
                    //console.log(params.data._id);
                    if (params.data.status == 'pending') {
                        return '<button class="btn btn-action" title="Approve"><i class="fa fa-check green"></i></button>';
                    }
                }
            },
            {
                headerName: "",
                field: "action",
                width: 35,
                cellClass: ['ag-cell-custom'],
                onCellClicked: function (params) {
                    _this.clickCell("reject", params.data);
                },
                cellRenderer: function (params) {
                    //console.log(params.data._id);
                    if (params.data.status == 'pending') {
                        return '<button class="btn btn-action" title="Approve"><i class="fa fa-close red"></i></button>';
                    }
                }
            },
        ];
    }
    PendingClaimComponent.prototype.ngOnInit = function () {
        this.getPendingClaim();
    };
    PendingClaimComponent.prototype.onGridReady = function (params) {
    };
    PendingClaimComponent.prototype.getPendingClaim = function () {
        var _this = this;
        this.claimRequestService.getPendingClaim().subscribe(function (res) {
            var response = res;
            //console.log(response);
            _this.rowData = response.data;
        });
    };
    PendingClaimComponent.prototype.clickCell = function (action, data) {
        var _this = this;
        if (data && (data.status == 'pending')) {
            data = { _id: data._id, status: action, updateddate: new Date() };
            //console.log(data)
            this.claimRequestService.claimAction(data).subscribe(function (res) {
                var response = res;
                //console.log(response);
                _this.getPendingClaim();
            });
        }
    };
    PendingClaimComponent.ctorParameters = function () { return [
        { type: _services_claim_request_service__WEBPACK_IMPORTED_MODULE_2__["ClaimRequestService"] }
    ]; };
    PendingClaimComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pending-claim',
            template: __webpack_require__(/*! raw-loader!./pending-claim.component.html */ "./node_modules/raw-loader/index.js!./src/app/pending-claim/pending-claim.component.html"),
            styles: [__webpack_require__(/*! ./pending-claim.component.css */ "./src/app/pending-claim/pending-claim.component.css")]
        })
    ], PendingClaimComponent);
    return PendingClaimComponent;
}());



/***/ }),

/***/ "./src/app/reimbursement-form/reimbursement-form.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/reimbursement-form/reimbursement-form.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "textarea{\r\n    height: 100px;\r\n}\r\n\r\n#exampleInputFile{\r\n    /* display: none; */\r\n}\r\n\r\ninput[type=file] {\r\n    position: absolute;\r\n    top: 0;\r\n    right: 0;\r\n    left: 0;\r\n    bottom: 0;\r\n    max-width: 100%;\r\n    max-height: 71%;\r\n    font-size: 100px;\r\n    text-align: right;\r\n    filter: alpha(opacity=0);\r\n    opacity: 0;\r\n    outline: none;   \r\n    cursor: inherit;\r\n    display: block;\r\n}\r\n\r\n.file-attach{\r\n    color: #fff;\r\n    background-color: #337ab7;\r\n    border-color: #2e6da4;\r\n}\r\n\r\n.help-block{\r\n    color: #e00707\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVpbWJ1cnNlbWVudC1mb3JtL3JlaW1idXJzZW1lbnQtZm9ybS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksYUFBYTtBQUNqQjs7QUFFQTtJQUNJLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixNQUFNO0lBQ04sUUFBUTtJQUNSLE9BQU87SUFDUCxTQUFTO0lBQ1QsZUFBZTtJQUNmLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLHdCQUF3QjtJQUN4QixVQUFVO0lBQ1YsYUFBYTtJQUNiLGVBQWU7SUFDZixjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksV0FBVztJQUNYLHlCQUF5QjtJQUN6QixxQkFBcUI7QUFDekI7O0FBR0E7SUFDSTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvcmVpbWJ1cnNlbWVudC1mb3JtL3JlaW1idXJzZW1lbnQtZm9ybS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGV4dGFyZWF7XHJcbiAgICBoZWlnaHQ6IDEwMHB4O1xyXG59XHJcblxyXG4jZXhhbXBsZUlucHV0RmlsZXtcclxuICAgIC8qIGRpc3BsYXk6IG5vbmU7ICovXHJcbn1cclxuXHJcbmlucHV0W3R5cGU9ZmlsZV0ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgbWF4LWhlaWdodDogNzElO1xyXG4gICAgZm9udC1zaXplOiAxMDBweDtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgZmlsdGVyOiBhbHBoYShvcGFjaXR5PTApO1xyXG4gICAgb3BhY2l0eTogMDtcclxuICAgIG91dGxpbmU6IG5vbmU7ICAgXHJcbiAgICBjdXJzb3I6IGluaGVyaXQ7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxuLmZpbGUtYXR0YWNoe1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzM3YWI3O1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjMmU2ZGE0O1xyXG59XHJcblxyXG5cclxuLmhlbHAtYmxvY2t7XHJcbiAgICBjb2xvcjogI2UwMDcwN1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/reimbursement-form/reimbursement-form.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/reimbursement-form/reimbursement-form.component.ts ***!
  \********************************************************************/
/*! exports provided: ReimbursementFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReimbursementFormComponent", function() { return ReimbursementFormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _services_claim_request_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/claim-request.service */ "./src/app/services/claim-request.service.ts");





var ReimbursementFormComponent = /** @class */ (function () {
    function ReimbursementFormComponent(fb, claim, el) {
        this.fb = fb;
        this.claim = claim;
        this.el = el;
        this.isSubmit = false;
        this.filesName = '';
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_3__["FileUploader"]({
            isHTML5: true
        });
    }
    ReimbursementFormComponent.prototype.ngOnInit = function () {
        this.uploadForm = this.fb.group({
            document: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            type: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            month: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            amount: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            comment: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])]
        });
    };
    ReimbursementFormComponent.prototype.uploadSubmit = function () {
        var _this = this;
        this.isSubmit = true;
        if (this.uploadForm.valid) {
            var inputEl_1 = this.el.nativeElement.querySelector('#document');
            var fileCount_1 = inputEl_1.files.length;
            // let formData = new FormData();
            var loggedInUser = JSON.parse(sessionStorage.loggedInUser).data;
            var datetime = new Date();
            var formData_1 = {
                type: this.uploadForm.controls.type.value,
                month: this.uploadForm.controls.month.value,
                amount: this.uploadForm.controls.amount.value,
                comment: this.uploadForm.controls.comment.value,
                status: "pending",
                user: loggedInUser.email,
                manager: loggedInUser.manager || null,
                docs: [],
                filename: [],
                createddate: datetime,
                updateddate: datetime
            };
            if (fileCount_1 && fileCount_1 > 0) { // a file was selected
                var fileSize = inputEl_1.files[0].size;
                if (fileSize > 0) {
                    var resolved_1 = 0;
                    var _loop_1 = function (id) {
                        var _id = Number(id);
                        var file = inputEl_1.files.item(Number(_id));
                        var myReader = new FileReader();
                        myReader.onloadend = function (file) {
                            if (inputEl_1.files[_id].name) {
                                var fileName = new Date().getTime() + "_" + _id + "_" + inputEl_1.files[_id].name;
                                formData_1.docs.push({ base64: myReader.result, type: file.type, "name": fileName });
                                formData_1.filename.push(fileName);
                                resolved_1++;
                                if ((fileCount_1) == Number(resolved_1)) {
                                    _this.claim.claimRequest(formData_1).subscribe(function (response) {
                                        _this.isSubmit = false;
                                        //console.log(response);
                                        if (response.status && response.status === 1) {
                                            _this.uploadForm.reset();
                                            _this.filesName = '';
                                            alert('Request submitted..');
                                        }
                                        else {
                                            _this.uploadForm.reset();
                                            _this.filesName = '';
                                            alert('something went wrong please after sometime or contact to admin..');
                                        }
                                    });
                                }
                            }
                        };
                        myReader.readAsDataURL(file);
                    };
                    for (var id in inputEl_1.files) {
                        _loop_1(id);
                    }
                }
            }
        }
    };
    ReimbursementFormComponent.prototype.uploadFile = function (data) {
        //console.log(data);
        if (data) {
            return data;
        }
        return "";
    };
    ReimbursementFormComponent.prototype.fileChange = function (event) {
        if (event.target.files.length === 1) {
            this.filesName = event.target.files[0].name;
        }
        else if (event.target.files.length > 1) {
            this.filesName = event.target.files.length + " files selected";
        }
    };
    ReimbursementFormComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _services_claim_request_service__WEBPACK_IMPORTED_MODULE_4__["ClaimRequestService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] }
    ]; };
    ReimbursementFormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-reimbursement-form',
            template: __webpack_require__(/*! raw-loader!./reimbursement-form.component.html */ "./node_modules/raw-loader/index.js!./src/app/reimbursement-form/reimbursement-form.component.html"),
            styles: [__webpack_require__(/*! ./reimbursement-form.component.css */ "./src/app/reimbursement-form/reimbursement-form.component.css")]
        })
    ], ReimbursementFormComponent);
    return ReimbursementFormComponent;
}());



/***/ }),

/***/ "./src/app/services/authentication.service.ts":
/*!****************************************************!*\
  !*** ./src/app/services/authentication.service.ts ***!
  \****************************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http, router) {
        this.http = http;
        this.router = router;
    }
    AuthenticationService.prototype.login = function (data) {
        var _this = this;
        var result;
        this.http.post('http://localhost:1520/api/auth/login/', data).subscribe(function (res) {
            result = res;
            if (result.status === 1) {
                sessionStorage.setItem('loggedInUser', JSON.stringify(result));
                var loggedInUser = JSON.parse(sessionStorage.getItem('loggedInUser'));
                if (result.data.role === 'admin' || result.data.role === 'manager') {
                    _this.router.navigate(['../pending-claim'], {});
                }
                else {
                    _this.router.navigate(['../my-claims'], {});
                }
            }
            else if (result.status === 0) {
                //console.log(result);
                alert(result.message);
            }
            else if (result.status === 2) {
                //console.log(result);
                alert(result.message);
            }
        });
    };
    AuthenticationService.prototype.signUp = function (data) {
        var _this = this;
        var result;
        this.http.post('http://localhost:1520/api/auth/signup/', data).subscribe(function (res) {
            result = res;
            if (result.status === 1) {
                //console.log('user created successfuly please login.. with same credential...');
                alert('user created..');
                _this.router.navigate(['../login'], {});
            }
            else if (result.status === 0) {
                alert(result.message);
                //console.log(result);
            }
            else if (result.status === 2) {
                //console.log(result);
                alert(result.message);
            }
        });
    };
    AuthenticationService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    AuthenticationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        })
    ], AuthenticationService);
    return AuthenticationService;
}());



/***/ }),

/***/ "./src/app/services/claim-request.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/claim-request.service.ts ***!
  \***************************************************/
/*! exports provided: ClaimRequestService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClaimRequestService", function() { return ClaimRequestService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var ClaimRequestService = /** @class */ (function () {
    function ClaimRequestService(http, router) {
        this.http = http;
        this.router = router;
    }
    ClaimRequestService.prototype.claimRequest = function (data) {
        var token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
        var headers_object = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set("Authorization", "Bearer " + token);
        var httpOptions = {
            headers: headers_object
        };
        return this.http.post('http://localhost:1520/api/claim/claimRequest/', data, httpOptions);
    };
    ClaimRequestService.prototype.getClaim = function () {
        var token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
        var headers_object = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set("Authorization", "Bearer " + token);
        var httpOptions = {
            headers: headers_object
        };
        return this.http.get('http://localhost:1520/api/claim/getClaim/' + JSON.parse(sessionStorage.getItem('loggedInUser')).data.email + '/' + JSON.parse(sessionStorage.getItem('loggedInUser')).data.role, httpOptions);
    };
    ClaimRequestService.prototype.getMyClaim = function () {
        var token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
        var headers_object = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set("Authorization", "Bearer " + token);
        var httpOptions = {
            headers: headers_object
        };
        return this.http.get('http://localhost:1520/api/claim/getMyClaim/' + JSON.parse(sessionStorage.getItem('loggedInUser')).data.email, httpOptions);
    };
    ClaimRequestService.prototype.getPendingClaim = function () {
        var token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
        var headers_object = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set("Authorization", "Bearer " + token);
        var httpOptions = {
            headers: headers_object
        };
        return this.http.get('http://localhost:1520/api/claim/getPendingClaim/' + JSON.parse(sessionStorage.getItem('loggedInUser')).data.email + '/' + JSON.parse(sessionStorage.getItem('loggedInUser')).data.role, httpOptions);
    };
    ClaimRequestService.prototype.claimAction = function (data) {
        var token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
        var headers_object = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set("Authorization", "Bearer " + token);
        var httpOptions = {
            headers: headers_object
        };
        return this.http.put('http://localhost:1520/api/claim/claimAction/', data, httpOptions);
    };
    ClaimRequestService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    ClaimRequestService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        })
    ], ClaimRequestService);
    return ClaimRequestService;
}());



/***/ }),

/***/ "./src/app/services/client.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/client.service.ts ***!
  \********************************************/
/*! exports provided: ClientService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientService", function() { return ClientService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var ClientService = /** @class */ (function () {
    function ClientService(http, router) {
        this.http = http;
        this.router = router;
    }
    ClientService.prototype.getUsers = function () {
        var token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
        var headers_object = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set("Authorization", "Bearer " + token);
        var httpOptions = {
            headers: headers_object
        };
        return this.http.get('http://localhost:1520/api/user/getusers/', httpOptions);
    };
    ClientService.prototype.allManager = function () {
        var token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
        var headers_object = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set("Authorization", "Bearer " + token);
        var httpOptions = {
            headers: headers_object
        };
        return this.http.get('http://localhost:1520/api/user/allManager/', httpOptions);
    };
    ClientService.prototype.addUser = function (data) {
        var token = JSON.parse(sessionStorage.getItem('loggedInUser')).token;
        var headers_object = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set("Authorization", "Bearer " + token);
        var httpOptions = {
            headers: headers_object
        };
        return this.http.post('http://localhost:1520/api/user/addUser/', data, httpOptions);
    };
    ClientService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
    ]; };
    ClientService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        })
    ], ClientService);
    return ClientService;
}());



/***/ }),

/***/ "./src/app/signup/signup.component.css":
/*!*********************************************!*\
  !*** ./src/app/signup/signup.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-control1 {\r\n    height: 28px;\r\n}\r\n\r\n.error {\r\n    font-size: 13px;\r\n    color: #e00707;\r\n    font-weight: 100;\r\n}\r\n\r\n.error-label{\r\n    height: 25px;\r\n}\r\n\r\n.login-mail {\r\n    margin-bottom: 0.6em;\r\n}\r\n\r\n.login-do{\r\n    padding: 22px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2lnbnVwL3NpZ251cC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksWUFBWTtBQUNoQjs7QUFFQTtJQUNJLGVBQWU7SUFDZixjQUFjO0lBQ2QsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksWUFBWTtBQUNoQjs7QUFFQTtJQUNJLG9CQUFvQjtBQUN4Qjs7QUFFQTtJQUNJLGFBQWE7QUFDakIiLCJmaWxlIjoic3JjL2FwcC9zaWdudXAvc2lnbnVwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9ybS1jb250cm9sMSB7XHJcbiAgICBoZWlnaHQ6IDI4cHg7XHJcbn1cclxuXHJcbi5lcnJvciB7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBjb2xvcjogI2UwMDcwNztcclxuICAgIGZvbnQtd2VpZ2h0OiAxMDA7XHJcbn1cclxuXHJcbi5lcnJvci1sYWJlbHtcclxuICAgIGhlaWdodDogMjVweDtcclxufVxyXG5cclxuLmxvZ2luLW1haWwge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMC42ZW07XHJcbn1cclxuXHJcbi5sb2dpbi1kb3tcclxuICAgIHBhZGRpbmc6IDIycHg7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/signup/signup.component.ts":
/*!********************************************!*\
  !*** ./src/app/signup/signup.component.ts ***!
  \********************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/authentication.service */ "./src/app/services/authentication.service.ts");




var SignupComponent = /** @class */ (function () {
    function SignupComponent(fb, auth) {
        this.fb = fb;
        this.auth = auth;
        this.isSubmit = false;
        this.confirmPassErr = false;
    }
    SignupComponent.prototype.ngOnInit = function () {
        this.signup = this.fb.group({
            password: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            repassword: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            // role:  [null, Validators.compose([Validators.required])],
            term: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            name: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
            email: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])]
        });
    };
    SignupComponent.prototype.signupSubmit = function () {
        this.isSubmit = true;
        if (this.signup.valid) {
            if (this.checkPasswords(this.signup)) {
                this.confirmPassErr = false;
                var datetime = new Date();
                var data = {
                    "name": this.signup.controls.name.value,
                    "email": this.signup.controls.email.value,
                    "role": "admin",
                    "password": this.signup.controls.password.value,
                    createddate: datetime,
                    updateddate: datetime
                };
                this.auth.signUp(data);
            }
            else {
                this.confirmPassErr = true;
            }
        }
    };
    SignupComponent.prototype.checkPasswords = function (data) {
        var pass = data.controls.password.value;
        var confirmPass = data.controls.repassword.value;
        return pass === confirmPass ? true : false;
    };
    SignupComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] }
    ]; };
    SignupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! raw-loader!./signup.component.html */ "./node_modules/raw-loader/index.js!./src/app/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ./signup.component.css */ "./src/app/signup/signup.component.css")]
        })
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! F:\Demo Projectcs\apiria\final\client\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map