const express = require('express');
const router = express.Router();
var userService = require('../services/users.service');

router.get('/getusers', getUser);
router.get('/allManager', allManager);
router.post('/addUser', addUser);
module.exports = router;


async function getUser(req, res) {
    try{
        let response = await userService.getusers(req);
        res.send(response);
    }catch(err){
        console.log(err);
    }
}


async function allManager(req, res) {
    try{
        let response = await userService.allManager(req);
        res.send(response);
    }catch(err){
        console.log(err);
    }
}


async function addUser(req, res) {
    try{
        let response = await userService.addUser(req);
        res.send(response);
    }catch(err){
        console.log(err);
    }
}

