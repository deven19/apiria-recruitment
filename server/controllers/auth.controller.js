
const express = require('express');
const router = express.Router();

var fs = require("fs");
var userService = require('../services/users.service');


//Define express route
router.post('/login', login);
router.post('/signUp', signUp);

router.post('/create', createUsers);
router.post('/claim', claimRequest);
router.get('/getuser/:email', getUser);
module.exports = router;


async function login(req, res) {
    try{
        let response = await userService.login(req);
        res.send(response);
    }catch(err){
        console.log(err);
    }
}

async function signUp(req, res) {
    try{
        let response = await userService.signUp(req);
        res.send(response);
    }catch(err){
        console.log(err);
    }
}


function createUsers(req,res){
    res.send({status : 0, message:"no user"});
}

function claimRequest(req, res){
    //console.log(req.body);
    let docs = req.body.docs;
    if(docs.length>0){
        for (var id in docs) {
            var file = docs[id].base64.split(';base64,').pop();
            var bitmap = new Buffer.from(file, 'base64');
            //console.log({name:docs[id].name, id:id});
            fs.writeFileSync("uploads/"+docs[id].name, bitmap,  {encoding: 'base64'}, (err, docs, id) => {
                console.log(err);
                //console.log(id);
                //console.log({name: docs.name})
            });
        }
    }
}

function getUser(req, res) {
    try {
        let search =  {email: {'$ne':req.params.email}};
        var db = global.dbconnections;
        db.collection('users', (err, collection)=> {
            collection.find(search).toArray((err, data)=> {
                if (err){
                    res.send({status : 10, message:"something went wrong please try again after sometime.."});
                };
                res.send({status : 1, data:data, message:"something went wrong please try again after sometime.."});
            });

        });
    }catch(err) {
        res.send({status : 10, message:"Invalid access..", error : err});
    }
}