const express = require('express');
const router = express.Router();
var claimService = require('../services/claim.service');

router.post('/claimRequest', claimRequest);
router.get('/getClaim/:email/:role', getClaim);
router.get('/getMyClaim/:email', getMyClaim);
router.get('/getPendingClaim/:email/:role', getPendingClaim);

router.put('/claimAction', claimAction);

module.exports = router;


async function claimRequest(req, res) {
    try{
        let response = await claimService.claimRequest(req);
        res.send(response);
    }catch(err){
        console.log(err);
    }
}

async function getClaim(req, res) {
    try{
        let response = await claimService.getClaim(req);
        res.send(response);
    }catch(err){
        console.log(err);
    }
}

async function getMyClaim(req, res) {
    try{
        let response = await claimService.getMyClaim(req);
        res.send(response);
    }catch(err){
        console.log(err);
    }
}


async function getPendingClaim(req, res) {
    try{
        let response = await claimService.getPendingClaim(req);
        res.send(response);
    }catch(err){
        console.log(err);
    }
}




async function claimAction(req, res) {
    try{
        let response = await claimService.claimAction(req);
        res.send(response);
    }catch(err){
        console.log(err);
    }
}

