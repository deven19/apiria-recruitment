var express = require("express");
var bodyparser = require("body-parser");
var path = require("path");
var cors = require("cors");
var mongodb = require('mongodb');
const jwt = require('jsonwebtoken');

const config = require('./config.json');
const auth = require('./controllers/auth.controller');
const users = require('./controllers/users.controller');
const claim = require('./controllers/claim.controller');
//var dbconnection = require('./services/connection.service');


var app = express();



//global connection
global.dbconnections;

//cors
app.use(cors());

app.use(express.urlencoded({limit: '50mb'}));

//body parser
app.use(express.json({limit: '50mb'}));


//satic files
app.use(express.static(path.join(__dirname, 'frontend')));

//call route
app.use('/api/auth', auth);
app.use('/api/user', validateToken, users);
app.use('/api/claim', validateToken, claim);

//initiate server
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/frontend/');
})

app.get('*', function(req, res){
    res.sendFile(__dirname + '/frontend/');
});

//for catching uncaughtException  
process.on('uncaughtException', (err)=> {
    console.log(err);
});

//for catching unhandledRejection 
process.on('unhandledRejection', (err)=>{
    console.log(err);
})



//Connect to DB
mongodb.MongoClient.connect(config.mongodb.url, { useNewUrlParser: true }, function(err, client) {
    if (err) {
        console.log({ result: 'connection error...', log: { error_name: err.name, error_message: err.message } });
    }
    if (client) {
        console.log("Connected to mongodb server");
        var db = client.db(config.mongodb.database);
        global.dbconnections = db;

    }
})

//Vakidate token
function validateToken(req, res, next) {
    try{
        var array = req.headers.authorization.split(' ');
        var token = jwt.verify(array[1],config.jwt_secret);
        next();
    }catch(err){
        res.send({status : 11, message:"Invalid access.."});
    }
}

//run node service
var server = app.listen(config.port, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("App listening at http://%s:%s", host, port)
 })